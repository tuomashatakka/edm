/* global describe, it, expect */

import edm from '../src'
import SerializerRegistry from '../src/forms/models/SerializerRegistry'
import { serializers } from '../src/forms'

describe("Serializer registry", function () {

  it('is initially empty', () => {
    const serializer = new SerializerRegistry()
    expect(serializer.serializers.length).toBe(0)
  })

  it('registers serializers successfully', () => {

    const serializer = new SerializerRegistry()
    serializer.register(serializers.TextInput)
    serializer.register(serializers.Checkbox)
    serializer.register(serializers.Radio)
    serializer.register(serializers.Select)

    const text = serializers.TextInput.type
    const checkbox = serializers.Checkbox.type
    const radio = serializers.Radio.type
    const select = serializers.Select.type

    expect(serializers.TextInput.type).toBe('text')
    expect(serializer.serializers.length).toBe(4)

    expect(serializer.serializers[0]).toBeDefined()
    expect(serializer.serializers[3]).toBeDefined()

    expect(serializer.serializers[0].type).toBe(text)
    expect(serializer.serializers[1].type).toBe(checkbox)
    expect(serializer.serializers[2].type).toBe(radio)
    expect(serializer.serializers[3].type).toBe(select)

  })

  it('resolves correctly', () => {

    const serializer = new SerializerRegistry()
    serializer.register(serializers.TextInput)
    serializer.register(serializers.Checkbox)
    serializer.register(serializers.Radio)
    serializer.register(serializers.Select)

    let ctr,
        field = document.createElement('input'),
        selectField = document.createElement('select')
    field.type = 'text'

    ctr = serializer.getForField(field)
    expect(ctr.name).toBe(serializers.TextInput.name)

    ctr = serializer.getForField(selectField)
    expect(ctr.name).toBe(serializers.Select.name)

    ctr = serializer.getForType('text')
    expect(ctr).not.toEqual(serializers.Select)

    ctr = serializer.getForType('checkbox')
    expect(ctr.name).toBe(serializers.Checkbox.name)
    expect(ctr.name).toBeDefined()

    ctr = serializer.getForType('radio')
    expect(ctr.name).toBe(serializers.Radio.name)
    expect(ctr).not.toEqual(serializers.TextInput)

    // Select is not a type attribute but a tag name instead
    ctr = serializer.getForType('select')
    expect(ctr).toBeUndefined()
  })


  // beforeEach(function () {
  //
  // })
  // let input = document.querySelector('input[type="radio"]')
  // let SerializerClass = serializers.getForField(input)
  //
  // let field = new SerializerClass(input)
  // let field.onDidChange((...args) => console.warn('change event', args[1].value))
})

describe ('Text input serializer', function () {

  const serializer = new SerializerRegistry()
  serializer.register(serializers.TextInput)

  let field = document.createElement('input')
  field.type = 'text'

  it('is fetched from the registry successfully', () => {
    let ø = serializer.getForField(field)
    expect(ø).toBeDefined()
  })

  it('is constructed successfully', () => {
    let ø = serializer.getForField(field)
    let s = new ø(field)
    expect(s).toBeDefined()
  })

  it("sets the field's value correctly", () => {
    let ø = serializer.getForField(field)
    let s = new ø(field)
    s.value = 'kakka'
    expect(field.value).toBe('kakka')
  })

  it("gets the field's value correctly", () => {
    let ø = new (serializer.getForField(field))(field)
    field.value = 'pissa'
    expect(ø.value).toBe('pissa')
  })

  it("subscribes to observe events", () => {
    // let ø = new (serializer.getForField(field))(field)
    // let i = 0
    // let callback = () => i++
    // ø.onDidChange(callback)
    // let spy = spyOn(callback)
    // expect(spy).not.toHaveBeenCalled()
    // ø.value = 'asdasd'
    // expect(i).toBe(1)
    // expect(spy).toHaveBeenCalledTimes(i)
  })

})
