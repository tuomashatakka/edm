/* global describe, it, expect, spyOn, beforeEach */

import * as bound from '../src/forms/adapter'
import { element } from '../src/dom'
import { TextInput } from '../src/forms/serializers'
import { SerializerRegistry, FormModel } from '../src/forms/models'

describe("Input adapter", function () {

  const registry = new SerializerRegistry()
  const getBoundForm  = bound.form.bind(registry)
  const getBoundField = bound.field.bind(registry)
  let form, field, fl, fm

  registry.register(TextInput)

  beforeEach(() => {

    fl = element('input')
    fm = element('form')
    fm.appendChild(fl)
    fl.setAttribute('name', 'sextual')

    form = getBoundForm(fm)
    field = getBoundField(fl)

  })


  it('binds form & field instances correctly', () => {

    expect(() => bound.form()).toThrowError()
    expect(() => bound.field()).toThrowError()
    expect(() => bound.form('form')).toThrowError()
    expect(() => bound.field('input')).toThrowError()
    expect(() => getBoundForm(fl)).toThrowError()
    expect(() => getBoundField(fl)).not.toThrowError()
    expect(() => getBoundForm(fm)).not.toThrowError()

    expect(form.constructor).toEqual(FormModel)
    expect(field.constructor.name).toBe(TextInput.name)

    form = getBoundForm('form')
    field = getBoundField('input')
    expect(form.constructor).toEqual(FormModel)
    expect(field.constructor.name).toBe(TextInput.name)
  })

  it('handles named fields', () => {

    expect(form.sextual).toBeDefined()
    expect(form.sextual.element).toBe(field.element)
    expect(form.sextual.value).toBe('')

    form.sextual = 'Jenkins, Mr.'
    expect(form.sextual.value).toBe('Jenkins, Mr.')
  })

  it('observes change events & fires handlers accordingly', () => {
    let set = field.__lookupSetter__('value')
    let get = field.__lookupGetter__('value')

    let changed = () => null
    let fld = { get, set, changed }

    spyOn(fld, "get").and.callThrough()
    spyOn(fld, "set").and.callThrough()
    spyOn(fld, "changed").and.callThrough()

    field.onDidChange(fld.changed)
    expect(fld.get).not.toHaveBeenCalled()
    expect(fld.set).not.toHaveBeenCalled()
    expect(fld.changed).not.toHaveBeenCalled()
    field.value = 'kok'

    expect(fld.changed).toHaveBeenCalledTimes(1)
    // expect(fld.changed).toHaveBeenCalledWith(field)
    //
    // fl.dispatchEvent(new Event('keyup', {key: 'b'}))
    //
    // console.log(field.value)
    // expect(fld.changed).toHaveBeenCalledTimes(2)
  })
  it('serializes data', () => {

    expect(form.fields.length).toBe(1)
    expect(form.serializedData).toEqual({ sextual: '' })

    field.value = 'kok'
    expect(form.serializedData).toEqual({ sextual: 'kok' })
  })
})
