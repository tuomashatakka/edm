/* global describe, it, expect, spyOn, beforeEach */
import * as dom from '../src/dom'
import { dispatch, clear } from '../src/events'
const dist = require('../dist/edm')

describe('DOM module', function () {
  it('contains the exported keys', function () {
    expect(Object.keys(dist.dom)).toContain('make')

    let dstKeys = Object.keys(dist.dom)
    let srcKeys = Object.keys(dom)
    expect(srcKeys.length).toEqual(dstKeys.length)
  })

  it('handles element creation successfully', function () {
    let srcel = dom.make('div')
    expect(srcel.tagName).toEqual('DIV')

    let el = dist.dom.make('div')
    expect(el.tagName).toEqual('DIV')
  })

  it('assigns attributes successfully', function () {
    for (let d of [dist.dom, dom]) {
      let called = 0
      let el = d.make({
        type: 'div',
        class: 'bro-division',
        onclick: () => called++,
        content: "xdls"
      })

      expect(el.classList).toContain('bro-division')
      expect(el.textContent).toEqual('xdls')

      // Check that the event listeners are assigned correctly
      expect(called).toBe(0)

      dispatch(el, "click")
      expect(called).toBe(1)

      dispatch(el, "keydown")
      expect(called).toBe(1)

      dispatch(el, "click")
      expect(called).toBe(2)

      d.attribute(el, "onclick", () => called++)
      expect(called).toBe(2)

      // Now there are two event handlers for the click event
      dispatch(el, "click")
      expect(called).toBe(4)
      expect(el.registeredEventHandlers["click"].length).toBe(2)

      // Finally, assert the listener cleanup works
      clear(el, "click")
      expect(el.registeredEventHandlers["click"].length).toBe(0)

      // Events should not be handled any longer
      dispatch(el, "click")
      expect(called).toBe(4)
    }
  })

  it('appends elements passed as the content argument', function () {
    for (let d of [dist.dom, dom]) {
      let child = d.make("span", "kikare")
      let el = d.make({
        type: 'div',
        content: [ child ]
      })
      expect(child.innerHTML).toEqual('kikare')
      expect(el.childElementCount).toEqual(1)
      expect(el.children[0]).toEqual(child)
      expect(el.children[1]).toBe(undefined)
      let el2 = d.make({
        type: 'div',
        content: [ child, child ]
      })
      expect(el.children[0]).toBe(undefined)
      expect(el2.children[0]).toBe(child)
      expect(el2.children[1]).toBe(undefined)
      let el3 = d.make({
        type: 'div',
        content: "<span>asd"
      })
      expect(el3.textContent).toBe("asd")
      expect(el3.innerHTML).toBe("<span>asd</span>")
      expect(el3.children[0].textContent).toBe("asd")
    }
  })
})
