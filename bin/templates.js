
const { readFileSync } = require('fs')
const { join } = require('path')
const DEFAULT_EXTENSION = '.html'
const VARIABLE_PATTERN  = /\{\{\s*(\w[^\s}]*)\s*\}\}/img


function translateContextVariables (_, variable) {
  let parts = variable.split('.')
  let iteratee = this.context
  let part
  while(iteratee && (part = parts.shift())) {
    iteratee = iteratee[part]
  }
  return iteratee
}


class Template {

  constructor (paths, ctx) {
    this.translate = translateContextVariables.bind(this)
    this._content  = ''
    this._source   = null
    let compiled   = false
    let publicPath = paths.public
    let assetsPath = paths.assets

    this.context = ctx
    this.state   = { publicPath, assetsPath, compiled }
  }

  set context (ctx) { this._context = ctx ? ctx : this._context || {} }

  get context () { return this._context }

  getFilePath (src) { return join(this.state.publicPath, src) }

  getAssetPath (src) {
    return join(this.state.assetsPath, src)
  }

  readSourceFile () {
    let fp = this.getFilePath(this._source)
    try {
      this._content = readFileSync(fp, 'utf8') }
    catch (e) {
      this._content = e.message.toString()
    }
    return this._content
  }

  read (page) {
    if (this.state.compiled)
      return this

    let src = page || this._source
    if (!src)
      throw new Error(`Source page is not defined`)

    // Append the default extension if there is none
    if (src.search(/\./) === -1)
      src += DEFAULT_EXTENSION

    this._source = src
    this.readSourceFile()
    return this
  }

  compile (page) {
    if (this.state.compiled)
      return this

    let src = page || this._source
    if (src)
      this.read(src)

    if (!this._context)
      throw new Error("Set the context before compiling")

    this._content = this._content.replace(VARIABLE_PATTERN, this.translate)
    this.state.compiled = true
    return this
  }

  render () {
    if (!this.state.compiled)
      throw new Error("Template must be compiled before it can be rendered")

    return this._content
  }
}

module.exports = Template
