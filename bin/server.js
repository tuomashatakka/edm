// let path = require('path')
const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const config = require('../conf/webpack.config.js')
const Template = require('./templates')
const { resolve } = require('path')

const PUBLIC_PATH = resolve(__dirname + '/../public')
const ASSETS_PATH = resolve(__dirname + '/../dist')
// const PUBLIC      = process.argv[3] || PUBLIC_PATH
// const ASSETS      = process.argv[4] || ASSETS_PATH || PUBLIC

const ADDR        = 'localhost'
const PORT        = process.argv[2] || process.env.PORT || 15517

let server        = express()
let router        = express.Router()
let compiler      = webpack(config)

function onInitializedHandler () {
  let details = {
    ADDRESS: `${ADDR}:${PORT}`,
    PUBLIC_PATH,
    ASSETS_PATH
  }
  // eslint-disable-next-line
  console.log(
    `>_ S E R V E R    R U N N I N G\n`,
    Object.keys(details).map(key => `\n    : ${key}${Array(40 - key.length).join(' ')}${details[key]}`).join(''),
    '\n\n    -   -   -\n\n'
  )
}

const { publicPath } = config.output
const options        = { publicPath, stats: { colors: true }}
const asset          = (route, type='js') => publicPath + '/' + route + '.' + type

router.get('/?:page', (request, response) => {

  let { page } = request.params
  let paths = {
    public: PUBLIC_PATH,
    assets: ASSETS_PATH,
  }
  let context = {
    page,
    assets: {
      main:   asset('edm', 'js'),
      style:   asset('style', 'css'),
    }
  }

  let template = new Template(paths, context)

  // eslint-disable-next-line
  console.log("-------------\nrequest.params\n", Object.keys(request.params), "\n", request.params)
  return response.send(template.compile(page).render())

})
// server.set('views', PUBLIC)
server.use('/assets', express.static(ASSETS_PATH))
// server.use('/src', express.static(ASSETS))
// server.use('/',    express.static(PUBLIC))
server.use(webpackDevMiddleware(compiler, options))
server.use(webpackHotMiddleware(compiler, { log: console.log })) // eslint-disable-line
server.use(router)
server.listen(PORT, onInitializedHandler)
