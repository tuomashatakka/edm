module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _disposableEvents = __webpack_require__(16);

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BaseFieldSerializer = (_temp = _class = function () {
  _createClass(BaseFieldSerializer, null, [{
    key: 'assert',
    value: function assert(field) {
      return field.tagName.toLowerCase() === this.tag && field.getAttribute('type') === this.type;
    }
  }]);

  function BaseFieldSerializer(field) {
    _classCallCheck(this, BaseFieldSerializer);

    this._element = field;
    this.subscriptions = new _disposableEvents.Collection();
  }

  _createClass(BaseFieldSerializer, [{
    key: 'destroy',
    value: function destroy() {
      this.subscriptions.dispose();
      this.element.remove();
    }
  }, {
    key: 'onChange',
    value: function onChange(handler) {
      var _this = this;

      return function (event) {
        return handler(event, _this);
      };
    }
  }, {
    key: 'observeEvent',
    value: function observeEvent() {
      return _observeEvent.apply(this, arguments);
    }
  }, {
    key: 'tagName',
    get: function get() {
      return this.constructor.tag;
    }
  }, {
    key: 'inputType',
    get: function get() {
      return this.constructor.type;
    }
  }, {
    key: 'element',
    get: function get() {
      return this._element;
    }
  }, {
    key: 'value',
    get: function get() {
      throw new Error('Subclasses of ' + this.constructor.name + ' must define a getter method for the `value` property');
    },
    set: function set(value) {
      throw new Error('Subclasses of ' + this.constructor.name + ' must define a setter method for the `value` property');
    }
  }]);

  return BaseFieldSerializer;
}(), _class.tag = null, _class.type = null, _temp);
exports.default = BaseFieldSerializer;


function _observeEvent(type, callback) {
  var _subscriptions;

  var disposables = void 0;
  var target = this.element;
  var handler = this.onChange(callback);
  if (target.length) target = [].concat(_toConsumableArray(target));
  if (target.map) disposables = target.map(function (el) {
    return new _disposableEvents.Event({ type: type, handler: handler, target: el });
  });else disposables = [new _disposableEvents.Event({ type: type, target: target, handler: handler })];
  (_subscriptions = this.subscriptions).add.apply(_subscriptions, _toConsumableArray(disposables));
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.throttle = throttle;
exports.assertChanged = assertChanged;

var DEFAULT_THROTTLE_INTERVAL = 550;

function throttle(callback, interval) {

  var timeout = null;
  interval = isNaN(parseInt(interval)) ? DEFAULT_THROTTLE_INTERVAL : interval;

  return function () {
    var _this = this,
        _arguments = arguments;

    var handler = function handler() {
      return callback.apply(_this, _arguments);
    };
    clearTimeout(timeout);
    timeout = setTimeout(handler, interval);
  };
}

function assertChanged(self, callback) {

  var _previousValue = null;

  return function () {

    var val = self.value;
    var doDispatch = true;

    if (typeof val === 'string') doDispatch = val !== _previousValue;else if (typeof val === 'boolean') doDispatch = val !== _previousValue;else doDispatch = !_previousValue || val && val.length != _previousValue.length;

    if (doDispatch) {
      _previousValue = val;
      return callback.apply(self, arguments);
    }
  };
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isTextInput = exports.isSelect = exports.isCheckbox = exports.isRadio = exports.toObject = exports.fields = exports.value = exports.css = exports.attributes = exports.attribute = exports.insert = exports.assertElement = exports.element = exports.make = undefined;

var _resolvers = __webpack_require__(9);

Object.defineProperty(exports, 'assertElement', {
  enumerable: true,
  get: function get() {
    return _resolvers.assertElement;
  }
});

var _assignment = __webpack_require__(5);

Object.defineProperty(exports, 'insert', {
  enumerable: true,
  get: function get() {
    return _assignment.insert;
  }
});
Object.defineProperty(exports, 'attribute', {
  enumerable: true,
  get: function get() {
    return _assignment.attribute;
  }
});
Object.defineProperty(exports, 'attributes', {
  enumerable: true,
  get: function get() {
    return _assignment.attributes;
  }
});
Object.defineProperty(exports, 'css', {
  enumerable: true,
  get: function get() {
    return _assignment.css;
  }
});

var _fields = __webpack_require__(12);

Object.defineProperty(exports, 'value', {
  enumerable: true,
  get: function get() {
    return _fields.value;
  }
});
Object.defineProperty(exports, 'fields', {
  enumerable: true,
  get: function get() {
    return _fields.fields;
  }
});
Object.defineProperty(exports, 'toObject', {
  enumerable: true,
  get: function get() {
    return _fields.toObject;
  }
});
Object.defineProperty(exports, 'isRadio', {
  enumerable: true,
  get: function get() {
    return _fields.isRadio;
  }
});
Object.defineProperty(exports, 'isCheckbox', {
  enumerable: true,
  get: function get() {
    return _fields.isCheckbox;
  }
});
Object.defineProperty(exports, 'isSelect', {
  enumerable: true,
  get: function get() {
    return _fields.isSelect;
  }
});
Object.defineProperty(exports, 'isTextInput', {
  enumerable: true,
  get: function get() {
    return _fields.isTextInput;
  }
});

var _make = __webpack_require__(13);

var _make2 = _interopRequireDefault(_make);

var _resolvers2 = _interopRequireDefault(_resolvers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.make = _make2.default;
exports.element = _resolvers2.default;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _AbstractBaseSerializer = __webpack_require__(0);

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseSelectSerializer = (_temp = _class = function (_Serializer) {
  _inherits(BaseSelectSerializer, _Serializer);

  function BaseSelectSerializer() {
    _classCallCheck(this, BaseSelectSerializer);

    return _possibleConstructorReturn(this, (BaseSelectSerializer.__proto__ || Object.getPrototypeOf(BaseSelectSerializer)).apply(this, arguments));
  }

  _createClass(BaseSelectSerializer, null, [{
    key: 'assert',
    value: function assert(field) {
      return this.tag === field.tagName.toLowerCase()
      // &&
      // this.multiple === !!field.getAttribute('multiple')
      ;
    }
  }]);

  return BaseSelectSerializer;
}(_AbstractBaseSerializer2.default), _class.tag = 'select', _class.multiple = false, _temp);
exports.default = BaseSelectSerializer;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SerializerRegistry = exports.FieldCollection = exports.FormModel = undefined;

var _FormModel = __webpack_require__(22);

var _FormModel2 = _interopRequireDefault(_FormModel);

var _FieldCollection = __webpack_require__(23);

var _FieldCollection2 = _interopRequireDefault(_FieldCollection);

var _SerializerRegistry = __webpack_require__(24);

var _SerializerRegistry2 = _interopRequireDefault(_SerializerRegistry);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.FormModel = _FormModel2.default;
exports.FieldCollection = _FieldCollection2.default;
exports.SerializerRegistry = _SerializerRegistry2.default;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.children = children;
exports.insert = insert;
exports.attribute = attribute;
exports.attributes = attributes;
exports.css = css;

var _events = __webpack_require__(6);

function children(el) {
  for (var _len = arguments.length, children = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    children[_key - 1] = arguments[_key];
  }

  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = children[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var child = _step.value;

      el.appendChild(child);
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return el;
}

function insert(el, p) {
  var n = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  var parent = p || document.body;

  if (n === null) return parent.appendChild(el);

  var children = parent.children;

  while (n < 0) {
    n += children.length;
  }n = n % children.length;

  parent.insertBefore(el, children[n]);
  return el;
}

/**
 * Set a value for an attribute for the given element.
 * If value is omitted, the attribute is removed from the
 * element.
 *
 * @method attribute
 *
 * @return {Element} Target element
 */

function attribute(el, key) {
  var val = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  if (val === null) el.removeAttribute(key);else if (key.startsWith('on')) (0, _events.listen)(el, key.substr(2), val);else el.setAttribute(key, val);

  return el;
}

/**
 * Map attributes to an element according to given object's
 * key-value pairs.
 *
 * Keys starting with 'on' are not added as attributes but as
 * event listeners instead. E.g. `onclick` will be translated
 * to element.addEventListener('click', attr['onclick']).
 *
 * @method attributes
 *
 * @param  {Element}  el    The targeted element
 * @param  {Object}   attr  Object containing the attributes that
 *                          should be assigned to the element
 * @return {Element}        Target element
 */

function attributes(el) {
  var attr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  Object.keys(attr).map(function (key) {
    return attribute(el, key, attr[key]);
  });
  return el;
}

function css(el) {
  var style = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  Object.keys(style).map(function (key) {
    return el.style.setProperty(key, style[key]);
  });
  return el;
}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dispatch = __webpack_require__(10);

Object.defineProperty(exports, "dispatch", {
  enumerable: true,
  get: function get() {
    return _dispatch.dispatch;
  }
});

var _listeners = __webpack_require__(11);

Object.defineProperty(exports, "clear", {
  enumerable: true,
  get: function get() {
    return _listeners.clear;
  }
});
Object.defineProperty(exports, "listen", {
  enumerable: true,
  get: function get() {
    return _listeners.listen;
  }
});

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _AbstractBaseSerializer = __webpack_require__(0);

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextInputSerializer = (_temp2 = _class = function (_Serializer) {
  _inherits(TextInputSerializer, _Serializer);

  function TextInputSerializer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, TextInputSerializer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = TextInputSerializer.__proto__ || Object.getPrototypeOf(TextInputSerializer)).call.apply(_ref, [this].concat(args))), _this), _this.onDidChange = function (callback) {
      return _this.observeEvent('keyup', (0, _utils.assertChanged)(_this, callback));
    }, _this.onDidStopChanging = function (callback) {
      return _this.observeEvent('keyup', (0, _utils.throttle)((0, _utils.assertChanged)(_this, callback)));
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(TextInputSerializer, [{
    key: 'value',
    get: function get() {
      return this.element.value;
    },
    set: function set(value) {
      this.element.value = value;
      this.element.dispatchEvent(new Event('keyup'));
    }
  }], [{
    key: 'assert',
    value: function assert(field) {
      var type = field.getAttribute('type');
      return field.tagName.toLowerCase() === this.tag && ['checkbox', 'radio'].indexOf(type) === -1;
    }
  }]);

  return TextInputSerializer;
}(_AbstractBaseSerializer2.default), _class.tag = 'input', _class.type = 'text', _temp2);
exports.default = TextInputSerializer;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _dom = __webpack_require__(2);

var dom = _interopRequireWildcard(_dom);

var _forms = __webpack_require__(14);

var forms = _interopRequireWildcard(_forms);

var _models = __webpack_require__(4);

var model = _interopRequireWildcard(_models);

var _events = __webpack_require__(6);

var event = _interopRequireWildcard(_events);

var _adapter = __webpack_require__(25);

var connect = _interopRequireWildcard(_adapter);

var _extend = __webpack_require__(26);

__webpack_require__(27);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var registry = new forms.SerializerRegistry();

var field = connect.field.bind(registry);
var form = connect.form.bind(registry);

var defaultSerializers = [_forms.serializers.TextInput, _forms.serializers.Textarea, _forms.serializers.Checkbox, _forms.serializers.Radio, _forms.serializers.Select];

var properties = {
  dom: _prop(dom),
  form: _prop(form),
  field: _prop(field),
  model: _prop(model),
  event: _prop(event),

  serializerTypes: _prop(_forms.serializers),
  createSerializer: _prop(_extend.createSerializer.bind(registry)),
  registerSerializer: _prop(registry.register.bind(registry))
};

function edm() {
  return form.apply(this, arguments);
}

function _prop(val) {
  return { value: val };
}

// Exports
registry.register.apply(registry, defaultSerializers);
Object.defineProperties(edm, properties);
module.exports = edm;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = element;
exports.assertElement = assertElement;
function element(query) {
  if (!query) return null;

  var el = void 0;
  var fn = [function (q) {
    return q.match(/<\w+>.*/) ? document.createElement(q.substr(q.indexOf('<'), q.indexOf(' ') - 1)) : null;
  }, function (q) {
    return q.match(/\w.*/) ? document.querySelector('*[name=\'' + q + '\']') : null;
  }, function (q) {
    return document.querySelector(q);
  }, function (q) {
    // eslint-disable-line
    try {
      return document.createElement(q);
    } catch (e) {
      throw new Error("Could not parse `element(" + query + ")`");
    }
  }];

  function test(q) {
    return fn.length ? fn.shift()(q) || test(q) : null;
  }

  if (typeof query === 'function') {
    el = query();
    if (el.tagName) return el;
  }

  if (typeof query.get === 'function') return query.get(0);

  if (query.tagName) return query;

  if (typeof query === 'string') return test(query.trim()) || null;else if (query.constructor.name.match(/^HTML\w+/)) return new query();

  return null;
}

function assertElement(query) {
  return element(query) ? true : false;
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dispatch = dispatch;

var EVENT_TYPES = {
  mouse: ["click", "dblclick", "mousedown", "mouseup", "mouseenter", "mouseleave", "mouseover", "mouseout", "mousemove", "mousewheel"],
  keyboard: ["keypress", "keydown", "keyup", ""]
};

var EVENT_RESOLVERS = {
  keyboard: initializeKeyboardEvent,
  generic: initializeGenericEvent,
  mouse: initializeMouseEvent
};

function dispatch(el, eventName) {
  var detail = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var trigger = initializeEvent(eventName);
  return trigger(el, detail);
}

// TODO: Polyfill
// if (document.createEvent) {
//   let event = new CustomEvent(eventName, { detail })
//   return el.dispatchEvent(event)
// }
// throw new TypeError("Polyfill missing")
// else {
// event = document.createEventObject()
// return el.fireEvent('on' + eventName, event)
// }

function initializeEvent(type) {
  var initializer = EVENT_RESOLVERS[Object.keys(EVENT_TYPES).reduce(function (resolvedType, key) {
    if (resolvedType !== null) return resolvedType;
    if (EVENT_TYPES[key].indexOf(type) > -1) return key;
  }, null)] || initializeGenericEvent;
  return initializer(type);
}

function initializeMouseEvent(type) {
  return function (el, detail) {
    var event = document.createEvent('MouseEvent', false, false, { detail: detail });
    event.initMouseEvent(type);
    el.dispatchEvent(event);
    return event;
  };
}

function initializeKeyboardEvent(type) {
  return function (el, detail) {
    var event = document.createEvent('KeyboardEvent', false, false, { detail: detail });
    event.initKeyboardEvent(type);
    el.dispatchEvent(event);
    return event;
  };
}

function initializeGenericEvent(type) {
  return function (el, detail) {
    // let event = document.createEvent('CustomEvent')
    var event = new CustomEvent(type, { detail: detail });

    event.initEvent(type);
    el.dispatchEvent(event);
    return event;
  };
}

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clear = clear;
exports.listen = listen;
function clear(el) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  if (!el || !el.registeredEventHandlers) return el;

  var remove = function remove(kind) {
    while (el.registeredEventHandlers[kind] && el.registeredEventHandlers[kind].length) {
      var handler = el.registeredEventHandlers[kind].pop();
      el.removeEventListener(kind, handler);
    }
  };
  if (type) remove(type);else Object.keys(el.registeredEventHandlers).forEach(function (kind) {
    return remove(kind);
  });
  return el;
}

function listen(el, type, handler) {
  if (!el) return null;
  assertListenerRegistryExists(el, type);
  registerHandler(el, type, handler);
  return el;
}

function registerHandler(el, type, handler) {
  el.registeredEventHandlers[type].push(handler);
  el.addEventListener(type, handler);
}

function assertListenerRegistryExists(el, type) {
  if (!el.clearListeners) el.clearListeners = function () {
    var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    return clear(el, type);
  };
  if (!el.registeredEventHandlers) el.registeredEventHandlers = {};
  if (!el.registeredEventHandlers[type]) el.registeredEventHandlers[type] = [];
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.value = value;
exports.fields = fields;
exports.toObject = toObject;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * DOM Utility functions
 * @module dom
 */

var INPUT_TAG_TEXT = 'input';
var INPUT_TAG_SELECT = 'select';
var INPUT_TYPE_RADIO = 'radio';
var INPUT_TYPE_CHECKBOX = 'checkbox';

var isRadio = exports.isRadio = function isRadio(_ref) {
  var type = _ref.type;
  return INPUT_TYPE_RADIO === type;
};
var isSelect = exports.isSelect = function isSelect(_ref2) {
  var tagName = _ref2.tagName;
  return INPUT_TAG_SELECT === tagName.toLowerCase();
};
var isCheckbox = exports.isCheckbox = function isCheckbox(_ref3) {
  var type = _ref3.type;
  return INPUT_TYPE_CHECKBOX === type;
};
var isTextInput = exports.isTextInput = function isTextInput(_ref4) {
  var type = _ref4.type,
      tagName = _ref4.tagName;
  return [INPUT_TYPE_RADIO, INPUT_TYPE_CHECKBOX].indexOf(type) === -1 && INPUT_TAG_TEXT === tagName.toLowerCase();
};

function value(el) {

  if (isCheckbox(el)) return Array.from(el.parentElement.parentElement.querySelectorAll('input[name="' + el.name + '"]')).filter(function (el) {
    return el.checked;
  }).map(function (el) {
    return el.value;
  });

  if (isRadio(el)) {
    var match = Array.from(el.parentElement.parentElement.querySelectorAll('input[name="' + el.name + '"]')).find(function (el) {
      return el.checked;
    });
    return match ? match.value : null;
  }

  if (isSelect(el)) return Array.from(el.selectedOptions).map(function (opt) {
    return opt.value;
  });

  return el.value;
}

function fields(el) {
  return Array.from(el.querySelectorAll('input, select, textarea'));
}

function toObject(el) {
  return fields(el).reduce(function (attr, field) {
    return _extends(attr, _defineProperty({}, field.name, value(field)));
  }, {});
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = make;

var _assignment = __webpack_require__(5);

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/**
 * Create a new element.
 *
 * @method make
 * @param  {String} type Type of the created element; or
 *                       html contents for a new div element
 *                       in case the second argument is omitted
 * @param  {String|Object} content Either a string with html contents for the created element; or
 *                                 an object of whose properties are assigned as attributes for
 *                                 the newly created element.
 * @return {Element} The element that was created
 */

function make(type, attrs) {
  var html = '';

  if (typeof attrs === 'undefined') {
    attrs = type;
    type = 'div';
  }

  if ((typeof attrs === 'undefined' ? 'undefined' : _typeof(attrs)) === 'object') {
    html = attrs.content || attrs.html || attrs.text || attrs.children;
    type = attrs.type || attrs.tag || attrs.tagName || type;
    var _arr = ['content', 'children', 'html', 'text', 'type', 'tag', 'tagName'];
    for (var _i = 0; _i < _arr.length; _i++) {
      var key = _arr[_i];
      if (attrs[key]) delete attrs[key];
    }
  }

  var el = document.createElement(type);

  if (typeof attrs === 'string') el.innerHTML = attrs;else {
    if (typeof html === 'string') el.innerHTML = html;else if (html instanceof HTMLCollection || html instanceof Array) _assignment.children.apply(undefined, [el].concat(_toConsumableArray(html)));

    if ((typeof attrs === 'undefined' ? 'undefined' : _typeof(attrs)) === 'object') return (0, _assignment.attributes)(el, attrs);
  }

  return el;
}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.serializers = exports.SerializerRegistry = exports.FieldCollection = exports.FormModel = undefined;

var _serializers = __webpack_require__(15);

var serializers = _interopRequireWildcard(_serializers);

var _models = __webpack_require__(4);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.FormModel = _models.FormModel;
exports.FieldCollection = _models.FieldCollection;
exports.SerializerRegistry = _models.SerializerRegistry;
exports.serializers = serializers;

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SelectMultiple = exports.Select = exports.Radio = exports.Checkbox = exports.Textarea = exports.TextInput = exports.AbstractBaseSelectSerializer = exports.AbstractBaseSerializer = exports.observeEvent = undefined;

var _AbstractBaseSerializer = __webpack_require__(0);

Object.defineProperty(exports, 'observeEvent', {
  enumerable: true,
  get: function get() {
    return _AbstractBaseSerializer.observeEvent;
  }
});

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

var _AbstractBaseSelectSerializer = __webpack_require__(3);

var _AbstractBaseSelectSerializer2 = _interopRequireDefault(_AbstractBaseSelectSerializer);

var _TextInputSerializer = __webpack_require__(7);

var _TextInputSerializer2 = _interopRequireDefault(_TextInputSerializer);

var _TextareaSerializer = __webpack_require__(17);

var _TextareaSerializer2 = _interopRequireDefault(_TextareaSerializer);

var _CheckboxInputSerializer = __webpack_require__(18);

var _CheckboxInputSerializer2 = _interopRequireDefault(_CheckboxInputSerializer);

var _RadioInputSerializer = __webpack_require__(19);

var _RadioInputSerializer2 = _interopRequireDefault(_RadioInputSerializer);

var _SelectInputSerializer = __webpack_require__(20);

var _SelectInputSerializer2 = _interopRequireDefault(_SelectInputSerializer);

var _MultiSelectInputSerializer = __webpack_require__(21);

var _MultiSelectInputSerializer2 = _interopRequireDefault(_MultiSelectInputSerializer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.AbstractBaseSerializer = _AbstractBaseSerializer2.default;
exports.AbstractBaseSelectSerializer = _AbstractBaseSelectSerializer2.default;
exports.TextInput = _TextInputSerializer2.default;
exports.Textarea = _TextareaSerializer2.default;
exports.Checkbox = _CheckboxInputSerializer2.default;
exports.Radio = _RadioInputSerializer2.default;
exports.Select = _SelectInputSerializer2.default;
exports.SelectMultiple = _MultiSelectInputSerializer2.default;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

!function(t,n){ true?module.exports=n():"function"==typeof define&&define.amd?define([],n):"object"==typeof exports?exports.disposable=n():t.disposable=n()}(this,function(){return function(t){function n(r){if(e[r])return e[r].exports;var o=e[r]={i:r,l:!1,exports:{}};return t[r].call(o.exports,o,o.exports,n),o.l=!0,o.exports}var e={};return n.m=t,n.c=e,n.d=function(t,e,r){n.o(t,e)||Object.defineProperty(t,e,{configurable:!1,enumerable:!0,get:r})},n.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return n.d(e,"a",e),e},n.o=function(t,n){return Object.prototype.hasOwnProperty.call(t,n)},n.p="",n(n.s=44)}([function(t,n){var e=t.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=e)},function(t,n){var e=t.exports={version:"2.4.0"};"number"==typeof __e&&(__e=e)},function(t,n,e){var r=e(9),o=e(32),i=e(17),u=Object.defineProperty;n.f=e(3)?Object.defineProperty:function(t,n,e){if(r(t),n=i(n,!0),r(e),o)try{return u(t,n,e)}catch(t){}if("get"in e||"set"in e)throw TypeError("Accessors not supported!");return"value"in e&&(t[n]=e.value),t}},function(t,n,e){t.exports=!e(11)(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},function(t,n){var e={}.hasOwnProperty;t.exports=function(t,n){return e.call(t,n)}},function(t,n,e){var r=e(61),o=e(18);t.exports=function(t){return r(o(t))}},function(t,n,e){var r=e(0),o=e(1),i=e(31),u=e(7),f=function(t,n,e){var c,s,a,l=t&f.F,p=t&f.G,d=t&f.S,y=t&f.P,v=t&f.B,h=t&f.W,b=p?o:o[n]||(o[n]={}),_=b.prototype,x=p?r:d?r[n]:(r[n]||{}).prototype;p&&(e=n);for(c in e)(s=!l&&x&&void 0!==x[c])&&c in b||(a=s?x[c]:e[c],b[c]=p&&"function"!=typeof x[c]?e[c]:v&&s?i(a,r):h&&x[c]==a?function(t){var n=function(n,e,r){if(this instanceof t){switch(arguments.length){case 0:return new t;case 1:return new t(n);case 2:return new t(n,e)}return new t(n,e,r)}return t.apply(this,arguments)};return n.prototype=t.prototype,n}(a):y&&"function"==typeof a?i(Function.call,a):a,y&&((b.virtual||(b.virtual={}))[c]=a,t&f.R&&_&&!_[c]&&u(_,c,a)))};f.F=1,f.G=2,f.S=4,f.P=8,f.B=16,f.W=32,f.U=64,f.R=128,t.exports=f},function(t,n,e){var r=e(2),o=e(12);t.exports=e(3)?function(t,n,e){return r.f(t,n,o(1,e))}:function(t,n,e){return t[n]=e,t}},function(t,n,e){var r=e(20)("wks"),o=e(13),i=e(0).Symbol,u="function"==typeof i;(t.exports=function(t){return r[t]||(r[t]=u&&i[t]||(u?i:o)("Symbol."+t))}).store=r},function(t,n,e){var r=e(10);t.exports=function(t){if(!r(t))throw TypeError(t+" is not an object!");return t}},function(t,n){t.exports=function(t){return"object"==typeof t?null!==t:"function"==typeof t}},function(t,n){t.exports=function(t){try{return!!t()}catch(t){return!0}}},function(t,n){t.exports=function(t,n){return{enumerable:!(1&t),configurable:!(2&t),writable:!(4&t),value:n}}},function(t,n){var e=0,r=Math.random();t.exports=function(t){return"Symbol(".concat(void 0===t?"":t,")_",(++e+r).toString(36))}},function(t,n,e){var r=e(39),o=e(25);t.exports=Object.keys||function(t){return r(t,o)}},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}function o(t){return t&&"function"==typeof t.dispose}Object.defineProperty(n,"__esModule",{value:!0}),n.isDisposable=n.default=void 0;var i=e(16),u=r(i),f=e(30),c=r(f),s=function(){function t(n){(0,u.default)(this,t),this.disposed=!1,this.disposalAction=n.bind(this)}return(0,c.default)(t,null,[{key:"isDisposable",value:function(t){return o(t)}}]),(0,c.default)(t,[{key:"dispose",value:function(){this.disposed||(this.disposed=!0,this.disposalAction&&this.disposalAction(),this.disposalAction=null)}}]),t}();n.default=s,n.isDisposable=o},function(t,n,e){"use strict";n.__esModule=!0,n.default=function(t,n){if(!(t instanceof n))throw new TypeError("Cannot call a class as a function")}},function(t,n,e){var r=e(10);t.exports=function(t,n){if(!r(t))return t;var e,o;if(n&&"function"==typeof(e=t.toString)&&!r(o=e.call(t)))return o;if("function"==typeof(e=t.valueOf)&&!r(o=e.call(t)))return o;if(!n&&"function"==typeof(e=t.toString)&&!r(o=e.call(t)))return o;throw TypeError("Can't convert object to primitive value")}},function(t,n){t.exports=function(t){if(void 0==t)throw TypeError("Can't call method on  "+t);return t}},function(t,n,e){var r=e(20)("keys"),o=e(13);t.exports=function(t){return r[t]||(r[t]=o(t))}},function(t,n,e){var r=e(0),o=r["__core-js_shared__"]||(r["__core-js_shared__"]={});t.exports=function(t){return o[t]||(o[t]={})}},function(t,n){var e=Math.ceil,r=Math.floor;t.exports=function(t){return isNaN(t=+t)?0:(t>0?r:e)(t)}},function(t,n){t.exports=!0},function(t,n){t.exports={}},function(t,n,e){var r=e(9),o=e(60),i=e(25),u=e(19)("IE_PROTO"),f=function(){},c=function(){var t,n=e(33)("iframe"),r=i.length;for(n.style.display="none",e(65).appendChild(n),n.src="javascript:",t=n.contentWindow.document,t.open(),t.write("<script>document.F=Object<\/script>"),t.close(),c=t.F;r--;)delete c.prototype[i[r]];return c()};t.exports=Object.create||function(t,n){var e;return null!==t?(f.prototype=r(t),e=new f,f.prototype=null,e[u]=t):e=c(),void 0===n?e:o(e,n)}},function(t,n){t.exports="constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",")},function(t,n,e){var r=e(2).f,o=e(4),i=e(8)("toStringTag");t.exports=function(t,n,e){t&&!o(t=e?t:t.prototype,i)&&r(t,i,{configurable:!0,value:n})}},function(t,n,e){n.f=e(8)},function(t,n,e){var r=e(0),o=e(1),i=e(22),u=e(27),f=e(2).f;t.exports=function(t){var n=o.Symbol||(o.Symbol=i?{}:r.Symbol||{});"_"==t.charAt(0)||t in n||f(n,t,{value:u.f(t)})}},function(t,n){n.f={}.propertyIsEnumerable},function(t,n,e){"use strict";n.__esModule=!0;var r=e(45),o=function(t){return t&&t.__esModule?t:{default:t}}(r);n.default=function(){function t(t,n){for(var e=0;e<n.length;e++){var r=n[e];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),(0,o.default)(t,r.key,r)}}return function(n,e,r){return e&&t(n.prototype,e),r&&t(n,r),n}}()},function(t,n,e){var r=e(48);t.exports=function(t,n,e){if(r(t),void 0===n)return t;switch(e){case 1:return function(e){return t.call(n,e)};case 2:return function(e,r){return t.call(n,e,r)};case 3:return function(e,r,o){return t.call(n,e,r,o)}}return function(){return t.apply(n,arguments)}}},function(t,n,e){t.exports=!e(3)&&!e(11)(function(){return 7!=Object.defineProperty(e(33)("div"),"a",{get:function(){return 7}}).a})},function(t,n,e){var r=e(10),o=e(0).document,i=r(o)&&r(o.createElement);t.exports=function(t){return i?o.createElement(t):{}}},function(t,n,e){var r=e(18);t.exports=function(t){return Object(r(t))}},function(t,n,e){var r=e(4),o=e(34),i=e(19)("IE_PROTO"),u=Object.prototype;t.exports=Object.getPrototypeOf||function(t){return t=o(t),r(t,i)?t[i]:"function"==typeof t.constructor&&t instanceof t.constructor?t.constructor.prototype:t instanceof Object?u:null}},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}n.__esModule=!0;var o=e(55),i=r(o),u=e(70),f=r(u),c="function"==typeof f.default&&"symbol"==typeof i.default?function(t){return typeof t}:function(t){return t&&"function"==typeof f.default&&t.constructor===f.default&&t!==f.default.prototype?"symbol":typeof t};n.default="function"==typeof f.default&&"symbol"===c(i.default)?function(t){return void 0===t?"undefined":c(t)}:function(t){return t&&"function"==typeof f.default&&t.constructor===f.default&&t!==f.default.prototype?"symbol":void 0===t?"undefined":c(t)}},function(t,n,e){"use strict";var r=e(22),o=e(6),i=e(38),u=e(7),f=e(4),c=e(23),s=e(59),a=e(26),l=e(35),p=e(8)("iterator"),d=!([].keys&&"next"in[].keys()),y=function(){return this};t.exports=function(t,n,e,v,h,b,_){s(e,n,v);var x,m,O,g=function(t){if(!d&&t in P)return P[t];switch(t){case"keys":case"values":return function(){return new e(this,t)}}return function(){return new e(this,t)}},w=n+" Iterator",j="values"==h,S=!1,P=t.prototype,E=P[p]||P["@@iterator"]||h&&P[h],M=E||g(h),k=h?j?g("entries"):M:void 0,A="Array"==n?P.entries||E:E;if(A&&(O=l(A.call(new t)))!==Object.prototype&&(a(O,w,!0),r||f(O,p)||u(O,p,y)),j&&E&&"values"!==E.name&&(S=!0,M=function(){return E.call(this)}),r&&!_||!d&&!S&&P[p]||u(P,p,M),c[n]=M,c[w]=y,h)if(x={values:j?M:g("values"),keys:b?M:g("keys"),entries:k},_)for(m in x)m in P||i(P,m,x[m]);else o(o.P+o.F*(d||S),n,x);return x}},function(t,n,e){t.exports=e(7)},function(t,n,e){var r=e(4),o=e(5),i=e(62)(!1),u=e(19)("IE_PROTO");t.exports=function(t,n){var e,f=o(t),c=0,s=[];for(e in f)e!=u&&r(f,e)&&s.push(e);for(;n.length>c;)r(f,e=n[c++])&&(~i(s,e)||s.push(e));return s}},function(t,n){var e={}.toString;t.exports=function(t){return e.call(t).slice(8,-1)}},function(t,n){n.f=Object.getOwnPropertySymbols},function(t,n,e){var r=e(39),o=e(25).concat("length","prototype");n.f=Object.getOwnPropertyNames||function(t){return r(t,o)}},function(t,n,e){var r=e(29),o=e(12),i=e(5),u=e(17),f=e(4),c=e(32),s=Object.getOwnPropertyDescriptor;n.f=e(3)?s:function(t,n){if(t=i(t),n=u(n,!0),c)try{return s(t,n)}catch(t){}if(f(t,n))return o(!r.f.call(t,n),t[n])}},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}function o(){return new(Function.prototype.bind.apply(a.default,[null].concat(Array.prototype.slice.call(arguments))))}function i(){return new(Function.prototype.bind.apply(c.default,[null].concat(Array.prototype.slice.call(arguments))))}function u(){return new(Function.prototype.bind.apply(p.default,[null].concat(Array.prototype.slice.call(arguments))))}Object.defineProperty(n,"__esModule",{value:!0}),n.Event=o,n.Instance=i,n.Collection=u;var f=e(15),c=r(f),s=e(49),a=r(s),l=e(89),p=r(l);n.default=t.exports},function(t,n,e){t.exports={default:e(46),__esModule:!0}},function(t,n,e){e(47);var r=e(1).Object;t.exports=function(t,n,e){return r.defineProperty(t,n,e)}},function(t,n,e){var r=e(6);r(r.S+r.F*!e(3),"Object",{defineProperty:e(2).f})},function(t,n){t.exports=function(t){if("function"!=typeof t)throw TypeError(t+" is not a function!");return t}},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(n,"__esModule",{value:!0}),n.default=void 0;var o=e(50),i=r(o),u=e(16),f=r(u),c=e(54),s=r(c),a=e(81),l=r(a),p=e(15),d=r(p),y=function(t,n){return t+" the listened event must be provided for the DisposableEvent by passing a `"+n+"` property upon calling its constructor"},v=y("The type of","type"),h=y("The handler for","handler"),b=function(t){function n(t){var e=t.type,r=t.handler,o=t.target;(0,f.default)(this,n);var u=function(){return c.target.removeEventListener(c.type,c.handler)};if(!r)throw new Error(h);if(!e)throw new Error(v);o||(o=document);var c=(0,s.default)(this,(n.__proto__||(0,i.default)(n)).call(this,u));return c.type=e||"click",c.target=o,c.handler=function(t){return r.call(o,t)},function(){c.disposed||c.target.addEventListener(c.type,c.handler)}(),c}return(0,l.default)(n,t),n}(d.default);n.default=b},function(t,n,e){t.exports={default:e(51),__esModule:!0}},function(t,n,e){e(52),t.exports=e(1).Object.getPrototypeOf},function(t,n,e){var r=e(34),o=e(35);e(53)("getPrototypeOf",function(){return function(t){return o(r(t))}})},function(t,n,e){var r=e(6),o=e(1),i=e(11);t.exports=function(t,n){var e=(o.Object||{})[t]||Object[t],u={};u[t]=n(e),r(r.S+r.F*i(function(){e(1)}),"Object",u)}},function(t,n,e){"use strict";n.__esModule=!0;var r=e(36),o=function(t){return t&&t.__esModule?t:{default:t}}(r);n.default=function(t,n){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!n||"object"!==(void 0===n?"undefined":(0,o.default)(n))&&"function"!=typeof n?t:n}},function(t,n,e){t.exports={default:e(56),__esModule:!0}},function(t,n,e){e(57),e(66),t.exports=e(27).f("iterator")},function(t,n,e){"use strict";var r=e(58)(!0);e(37)(String,"String",function(t){this._t=String(t),this._i=0},function(){var t,n=this._t,e=this._i;return e>=n.length?{value:void 0,done:!0}:(t=r(n,e),this._i+=t.length,{value:t,done:!1})})},function(t,n,e){var r=e(21),o=e(18);t.exports=function(t){return function(n,e){var i,u,f=String(o(n)),c=r(e),s=f.length;return c<0||c>=s?t?"":void 0:(i=f.charCodeAt(c),i<55296||i>56319||c+1===s||(u=f.charCodeAt(c+1))<56320||u>57343?t?f.charAt(c):i:t?f.slice(c,c+2):u-56320+(i-55296<<10)+65536)}}},function(t,n,e){"use strict";var r=e(24),o=e(12),i=e(26),u={};e(7)(u,e(8)("iterator"),function(){return this}),t.exports=function(t,n,e){t.prototype=r(u,{next:o(1,e)}),i(t,n+" Iterator")}},function(t,n,e){var r=e(2),o=e(9),i=e(14);t.exports=e(3)?Object.defineProperties:function(t,n){o(t);for(var e,u=i(n),f=u.length,c=0;f>c;)r.f(t,e=u[c++],n[e]);return t}},function(t,n,e){var r=e(40);t.exports=Object("z").propertyIsEnumerable(0)?Object:function(t){return"String"==r(t)?t.split(""):Object(t)}},function(t,n,e){var r=e(5),o=e(63),i=e(64);t.exports=function(t){return function(n,e,u){var f,c=r(n),s=o(c.length),a=i(u,s);if(t&&e!=e){for(;s>a;)if((f=c[a++])!=f)return!0}else for(;s>a;a++)if((t||a in c)&&c[a]===e)return t||a||0;return!t&&-1}}},function(t,n,e){var r=e(21),o=Math.min;t.exports=function(t){return t>0?o(r(t),9007199254740991):0}},function(t,n,e){var r=e(21),o=Math.max,i=Math.min;t.exports=function(t,n){return t=r(t),t<0?o(t+n,0):i(t,n)}},function(t,n,e){t.exports=e(0).document&&document.documentElement},function(t,n,e){e(67);for(var r=e(0),o=e(7),i=e(23),u=e(8)("toStringTag"),f=["NodeList","DOMTokenList","MediaList","StyleSheetList","CSSRuleList"],c=0;c<5;c++){var s=f[c],a=r[s],l=a&&a.prototype;l&&!l[u]&&o(l,u,s),i[s]=i.Array}},function(t,n,e){"use strict";var r=e(68),o=e(69),i=e(23),u=e(5);t.exports=e(37)(Array,"Array",function(t,n){this._t=u(t),this._i=0,this._k=n},function(){var t=this._t,n=this._k,e=this._i++;return!t||e>=t.length?(this._t=void 0,o(1)):"keys"==n?o(0,e):"values"==n?o(0,t[e]):o(0,[e,t[e]])},"values"),i.Arguments=i.Array,r("keys"),r("values"),r("entries")},function(t,n){t.exports=function(){}},function(t,n){t.exports=function(t,n){return{value:n,done:!!t}}},function(t,n,e){t.exports={default:e(71),__esModule:!0}},function(t,n,e){e(72),e(78),e(79),e(80),t.exports=e(1).Symbol},function(t,n,e){"use strict";var r=e(0),o=e(4),i=e(3),u=e(6),f=e(38),c=e(73).KEY,s=e(11),a=e(20),l=e(26),p=e(13),d=e(8),y=e(27),v=e(28),h=e(74),b=e(75),_=e(76),x=e(9),m=e(5),O=e(17),g=e(12),w=e(24),j=e(77),S=e(43),P=e(2),E=e(14),M=S.f,k=P.f,A=j.f,F=r.Symbol,T=r.JSON,I=T&&T.stringify,N=d("_hidden"),C=d("toPrimitive"),D={}.propertyIsEnumerable,L=a("symbol-registry"),R=a("symbols"),W=a("op-symbols"),J=Object.prototype,G="function"==typeof F,K=r.QObject,z=!K||!K.prototype||!K.prototype.findChild,B=i&&s(function(){return 7!=w(k({},"a",{get:function(){return k(this,"a",{value:7}).a}})).a})?function(t,n,e){var r=M(J,n);r&&delete J[n],k(t,n,e),r&&t!==J&&k(J,n,r)}:k,Y=function(t){var n=R[t]=w(F.prototype);return n._k=t,n},Q=G&&"symbol"==typeof F.iterator?function(t){return"symbol"==typeof t}:function(t){return t instanceof F},U=function(t,n,e){return t===J&&U(W,n,e),x(t),n=O(n,!0),x(e),o(R,n)?(e.enumerable?(o(t,N)&&t[N][n]&&(t[N][n]=!1),e=w(e,{enumerable:g(0,!1)})):(o(t,N)||k(t,N,g(1,{})),t[N][n]=!0),B(t,n,e)):k(t,n,e)},q=function(t,n){x(t);for(var e,r=b(n=m(n)),o=0,i=r.length;i>o;)U(t,e=r[o++],n[e]);return t},H=function(t,n){return void 0===n?w(t):q(w(t),n)},V=function(t){var n=D.call(this,t=O(t,!0));return!(this===J&&o(R,t)&&!o(W,t))&&(!(n||!o(this,t)||!o(R,t)||o(this,N)&&this[N][t])||n)},X=function(t,n){if(t=m(t),n=O(n,!0),t!==J||!o(R,n)||o(W,n)){var e=M(t,n);return!e||!o(R,n)||o(t,N)&&t[N][n]||(e.enumerable=!0),e}},Z=function(t){for(var n,e=A(m(t)),r=[],i=0;e.length>i;)o(R,n=e[i++])||n==N||n==c||r.push(n);return r},$=function(t){for(var n,e=t===J,r=A(e?W:m(t)),i=[],u=0;r.length>u;)!o(R,n=r[u++])||e&&!o(J,n)||i.push(R[n]);return i};G||(F=function(){if(this instanceof F)throw TypeError("Symbol is not a constructor!");var t=p(arguments.length>0?arguments[0]:void 0),n=function(e){this===J&&n.call(W,e),o(this,N)&&o(this[N],t)&&(this[N][t]=!1),B(this,t,g(1,e))};return i&&z&&B(J,t,{configurable:!0,set:n}),Y(t)},f(F.prototype,"toString",function(){return this._k}),S.f=X,P.f=U,e(42).f=j.f=Z,e(29).f=V,e(41).f=$,i&&!e(22)&&f(J,"propertyIsEnumerable",V,!0),y.f=function(t){return Y(d(t))}),u(u.G+u.W+u.F*!G,{Symbol:F});for(var tt="hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","),nt=0;tt.length>nt;)d(tt[nt++]);for(var tt=E(d.store),nt=0;tt.length>nt;)v(tt[nt++]);u(u.S+u.F*!G,"Symbol",{for:function(t){return o(L,t+="")?L[t]:L[t]=F(t)},keyFor:function(t){if(Q(t))return h(L,t);throw TypeError(t+" is not a symbol!")},useSetter:function(){z=!0},useSimple:function(){z=!1}}),u(u.S+u.F*!G,"Object",{create:H,defineProperty:U,defineProperties:q,getOwnPropertyDescriptor:X,getOwnPropertyNames:Z,getOwnPropertySymbols:$}),T&&u(u.S+u.F*(!G||s(function(){var t=F();return"[null]"!=I([t])||"{}"!=I({a:t})||"{}"!=I(Object(t))})),"JSON",{stringify:function(t){if(void 0!==t&&!Q(t)){for(var n,e,r=[t],o=1;arguments.length>o;)r.push(arguments[o++]);return n=r[1],"function"==typeof n&&(e=n),!e&&_(n)||(n=function(t,n){if(e&&(n=e.call(this,t,n)),!Q(n))return n}),r[1]=n,I.apply(T,r)}}}),F.prototype[C]||e(7)(F.prototype,C,F.prototype.valueOf),l(F,"Symbol"),l(Math,"Math",!0),l(r.JSON,"JSON",!0)},function(t,n,e){var r=e(13)("meta"),o=e(10),i=e(4),u=e(2).f,f=0,c=Object.isExtensible||function(){return!0},s=!e(11)(function(){return c(Object.preventExtensions({}))}),a=function(t){u(t,r,{value:{i:"O"+ ++f,w:{}}})},l=function(t,n){if(!o(t))return"symbol"==typeof t?t:("string"==typeof t?"S":"P")+t;if(!i(t,r)){if(!c(t))return"F";if(!n)return"E";a(t)}return t[r].i},p=function(t,n){if(!i(t,r)){if(!c(t))return!0;if(!n)return!1;a(t)}return t[r].w},d=function(t){return s&&y.NEED&&c(t)&&!i(t,r)&&a(t),t},y=t.exports={KEY:r,NEED:!1,fastKey:l,getWeak:p,onFreeze:d}},function(t,n,e){var r=e(14),o=e(5);t.exports=function(t,n){for(var e,i=o(t),u=r(i),f=u.length,c=0;f>c;)if(i[e=u[c++]]===n)return e}},function(t,n,e){var r=e(14),o=e(41),i=e(29);t.exports=function(t){var n=r(t),e=o.f;if(e)for(var u,f=e(t),c=i.f,s=0;f.length>s;)c.call(t,u=f[s++])&&n.push(u);return n}},function(t,n,e){var r=e(40);t.exports=Array.isArray||function(t){return"Array"==r(t)}},function(t,n,e){var r=e(5),o=e(42).f,i={}.toString,u="object"==typeof window&&window&&Object.getOwnPropertyNames?Object.getOwnPropertyNames(window):[],f=function(t){try{return o(t)}catch(t){return u.slice()}};t.exports.f=function(t){return u&&"[object Window]"==i.call(t)?f(t):o(r(t))}},function(t,n){},function(t,n,e){e(28)("asyncIterator")},function(t,n,e){e(28)("observable")},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}n.__esModule=!0;var o=e(82),i=r(o),u=e(86),f=r(u),c=e(36),s=r(c);n.default=function(t,n){if("function"!=typeof n&&null!==n)throw new TypeError("Super expression must either be null or a function, not "+(void 0===n?"undefined":(0,s.default)(n)));t.prototype=(0,f.default)(n&&n.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),n&&(i.default?(0,i.default)(t,n):t.__proto__=n)}},function(t,n,e){t.exports={default:e(83),__esModule:!0}},function(t,n,e){e(84),t.exports=e(1).Object.setPrototypeOf},function(t,n,e){var r=e(6);r(r.S,"Object",{setPrototypeOf:e(85).set})},function(t,n,e){var r=e(10),o=e(9),i=function(t,n){if(o(t),!r(n)&&null!==n)throw TypeError(n+": can't set as prototype!")};t.exports={set:Object.setPrototypeOf||("__proto__"in{}?function(t,n,r){try{r=e(31)(Function.call,e(43).f(Object.prototype,"__proto__").set,2),r(t,[]),n=!(t instanceof Array)}catch(t){n=!0}return function(t,e){return i(t,e),n?t.__proto__=e:r(t,e),t}}({},!1):void 0),check:i}},function(t,n,e){t.exports={default:e(87),__esModule:!0}},function(t,n,e){e(88);var r=e(1).Object;t.exports=function(t,n){return r.create(t,n)}},function(t,n,e){var r=e(6);r(r.S,"Object",{create:e(24)})},function(t,n,e){"use strict";function r(t){return t&&t.__esModule?t:{default:t}}Object.defineProperty(n,"__esModule",{value:!0}),n.default=void 0;var o=e(16),i=r(o),u=e(30),f=r(u),c=e(15),s=function(){function t(){(0,i.default)(this,t),this.disposables=[],this.disposed=!1,this.add.apply(this,arguments)}return(0,f.default)(t,[{key:"add",value:function(){var t=this;if(!this.disposed){for(var n=arguments.length,e=Array(n),r=0;r<n;r++)e[r]=arguments[r];e.forEach(function(n){if(!(0,c.isDisposable)(n))throw new Error("Parameters to DisposableCollection.add should have a dispose method");t.disposables.push(n)})}}},{key:"remove",value:function(){var t=this;if(!this.disposed){for(var n=arguments.length,e=Array(n),r=0;r<n;r++)e[r]=arguments[r];e.forEach(function(n){var e=t.disposables.findIndex(function(t){return t==n});e>-1&&t.disposables.splice(e,1)})}}},{key:"clear",value:function(){this.disposed||(this.disposables=[])}},{key:"dispose",value:function(){this.disposed||(this.disposed=!0,this.disposables.forEach(function(t){return t.dispose()}),this.disposables=null)}}]),t}();n.default=s}])});

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp;

var _TextInputSerializer2 = __webpack_require__(7);

var _TextInputSerializer3 = _interopRequireDefault(_TextInputSerializer2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TextareaSerializer = (_temp = _class = function (_TextInputSerializer) {
  _inherits(TextareaSerializer, _TextInputSerializer);

  function TextareaSerializer() {
    _classCallCheck(this, TextareaSerializer);

    return _possibleConstructorReturn(this, (TextareaSerializer.__proto__ || Object.getPrototypeOf(TextareaSerializer)).apply(this, arguments));
  }

  _createClass(TextareaSerializer, null, [{
    key: 'assert',
    value: function assert(field) {
      var type = field.getAttribute('type');
      return field.tagName.toLowerCase() === this.tag && type === null;
    }
  }]);

  return TextareaSerializer;
}(_TextInputSerializer3.default), _class.tag = 'textarea', _temp);
exports.default = TextareaSerializer;

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _AbstractBaseSerializer = __webpack_require__(0);

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CheckboxInputSerializer = (_temp2 = _class = function (_Serializer) {
  _inherits(CheckboxInputSerializer, _Serializer);

  function CheckboxInputSerializer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, CheckboxInputSerializer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = CheckboxInputSerializer.__proto__ || Object.getPrototypeOf(CheckboxInputSerializer)).call.apply(_ref, [this].concat(args))), _this), _this.onDidChange = function (callback) {
      return _this.observeEvent('click', (0, _utils.assertChanged)(_this, callback));
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(CheckboxInputSerializer, [{
    key: 'element',


    /**
     * Returns all checkbox inputs within the given field's form. If the input has
     * no form, whole document is used to search for the inputs.
     * @method element
     * @return {Array} List of all related checkbox input elements
     */

    get: function get() {
      var _element = this._element,
          name = _element.name,
          form = _element.form;
      var type = this.constructor.type;

      return form.querySelectorAll('input[type="' + type + '"][name="' + name + '"]');
    }

    /**
     * Returns a list of values within related input elements
     * @method options
     * @return {[type]} [description]
     */

  }, {
    key: 'options',
    get: function get() {
      return Array.from(this.element).map(function (el) {
        return el.value;
      });
    }

    /**
     * Value getter. Returns an array containing values for each active checkbox within related inputs
     * @method value
     * @return {Array} List of active checkboxes' values
     */

  }, {
    key: 'value',
    get: function get() {
      return Array.from(this.element).filter(function (el) {
        return el.checked;
      }).map(function (el) {
        return el.value;
      });
    }

    /**
     * Value setter. Changes the related checkboxes' states according to provided value
     *
     * @method value
     * @param  {array|string|boolean} value New value for the related checkbox inputs
     *                                      Either an array containing values for the active checkboxes
     *                                      or string for toggling only one checkbox
     *                                      or boolean to toggle uniform checked state for each checkbox
     */

    ,
    set: function set(value) {
      var fn = void 0;
      switch (typeof value === 'undefined' ? 'undefined' : _typeof(value)) {
        case 'boolean':
          fn = function fn() {
            return value;
          };break;
        case 'string':
          fn = function fn(current) {
            return current === value;
          };break;
        default:
          fn = function fn(current) {
            return value.indexOf(current) > -1;
          };break;
      }
      this.element.forEach(function (el) {
        return el.checked = fn(el.value);
      });
    }

    /**
     * Register a change listener that is triggered whenever the value for this field changes
     * @method onDidChange
     * @param  {Function}  callback [description]
     * @return {[type]}    [description]
     */

  }]);

  return CheckboxInputSerializer;
}(_AbstractBaseSerializer2.default), _class.tag = 'input', _class.type = 'checkbox', _temp2);
exports.default = CheckboxInputSerializer;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _AbstractBaseSerializer = __webpack_require__(0);

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var RadioInputSerializer = (_temp2 = _class = function (_Serializer) {
  _inherits(RadioInputSerializer, _Serializer);

  function RadioInputSerializer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, RadioInputSerializer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = RadioInputSerializer.__proto__ || Object.getPrototypeOf(RadioInputSerializer)).call.apply(_ref, [this].concat(args))), _this), _this.onDidChange = function (callback) {
      return _this.observeEvent('click', (0, _utils.assertChanged)(_this, callback));
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(RadioInputSerializer, [{
    key: 'element',


    /**
     * Returns all radio inputs within the given field's form. If the input has
     * no form, whole document is used to search for the inputs.
     * @method element
     * @return {Array} List of all related checkbox input elements
     */

    get: function get() {
      var type = this.constructor.type;
      var _element = this._element,
          name = _element.name,
          form = _element.form;


      return (form || document.body).querySelectorAll('input[type="' + type + '"][name="' + name + '"]');
    }

    /**
     * Returns a list of values within related input elements
     * @method options
     * @return {[type]} [description]
     */

  }, {
    key: 'options',
    get: function get() {
      return Array.from(this.element).map(function (el) {
        return el.value;
      });
    }

    /**
     * Value getter. Returns an array containing values for each active checkbox within related inputs
     * @method value
     * @return {Array} List of active checkboxes' values
     */

  }, {
    key: 'value',
    get: function get() {
      var active = Array.from(this.element).filter(function (el) {
        return el.checked;
      });
      if (active.length) return active[0].value;
      return null;
    }

    /**
     * Value setter. Changes the related checkboxes' states according to provided value
     *
     * @method value
     * @param  {string|number} value New value for the field
     *                               Either the value for the input that should be marked active
     *                               or a number describing the zero-based index for the input to be marked active
     *                               On any other case all inputs are set unselected
     */

    ,
    set: function set(value) {
      var fn = void 0;
      switch (typeof value === 'undefined' ? 'undefined' : _typeof(value)) {
        case 'number':
          fn = function fn(el, n) {
            return n === value;
          };break;
        case 'string':
          fn = function fn(current) {
            return current === value;
          };break;
        default:
          fn = function fn() {
            return false;
          };break;
      }
      this.element.forEach(function (el, n) {
        return el.checked = fn(el.value, n);
      });
    }

    /**
     * Register a change listener that is triggered whenever the value for this field changes
     * @method onDidChange
     * @param  {Function}  callback [description]
     * @return {[type]}    [description]
     */

  }]);

  return RadioInputSerializer;
}(_AbstractBaseSerializer2.default), _class.tag = 'input', _class.type = 'radio', _temp2);
exports.default = RadioInputSerializer;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _AbstractBaseSelectSerializer = __webpack_require__(3);

var _AbstractBaseSelectSerializer2 = _interopRequireDefault(_AbstractBaseSelectSerializer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SelectInputSerializer = function (_BaseSelectSerializer) {
  _inherits(SelectInputSerializer, _BaseSelectSerializer);

  function SelectInputSerializer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, SelectInputSerializer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = SelectInputSerializer.__proto__ || Object.getPrototypeOf(SelectInputSerializer)).call.apply(_ref, [this].concat(args))), _this), _this.onDidChange = function (callback) {
      return _this.observeEvent('change', (0, _utils.assertChanged)(_this, callback));
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(SelectInputSerializer, [{
    key: 'options',


    /**
     * Returns a list of values within related input elements
     * @method options
     * @return {[type]} [description]
     */

    get: function get() {
      return Array.from(this.element.options).map(function (option) {
        return option.value;
      });
    }

    /**
     * Value getter. Returns an array containing all selected options
     * @method value
     * @return {Array} List of active checkboxes' values
     */

  }, {
    key: 'value',
    get: function get() {
      return Array.from(this.element.selectedOptions).map(function (opt) {
        return opt.value;
      });
    }

    /**
     * Value setter. Selects the given options.
     * Any argument value that evaluates to false will clear the selection.
     *
     * @method value
     * @param  {array|string|boolean} value
     *         New value for the related checkbox inputs. Either
     *          - an array containing values for the active options; or
     *          - string for toggling only one option.
     */

    ,
    set: function set(value) {
      var optEls = [].concat(_toConsumableArray(this.element.options));
      var fn = void 0;

      if (!value) fn = fn = function fn() {
        return value;
      };else switch (typeof value === 'undefined' ? 'undefined' : _typeof(value)) {
        case 'boolean':
          fn = function fn() {
            return value;
          };break;
        case 'number':
          fn = function fn(cr, n) {
            return value === n;
          };break;
        case 'string':
          fn = function fn(current) {
            return current === value;
          };break;
        case 'object':
          fn = function fn(current) {
            return resolveObject(value, current);
          };break;
        default:
          fn = function fn() {
            return false;
          };
      }

      optEls.forEach(function (el) {
        return el.selected = fn(el.value);
      });
    }

    /**
     * Register a change listener that is triggered whenever the value for this field changes
     * @method onDidChange
     * @param  {Function}  callback [description]
     * @return {[type]}    [description]
     */

  }]);

  return SelectInputSerializer;
}(_AbstractBaseSelectSerializer2.default);

exports.default = SelectInputSerializer;


function resolveObject(value, current) {
  if (value.constructor.name === 'Array') return value.indexOf(current) > -1;
  return value[current];
}

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _AbstractBaseSelectSerializer = __webpack_require__(3);

var _AbstractBaseSelectSerializer2 = _interopRequireDefault(_AbstractBaseSelectSerializer);

var _utils = __webpack_require__(1);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MultiSelectInputSerializer = (_temp2 = _class = function (_BaseSelectSerializer) {
  _inherits(MultiSelectInputSerializer, _BaseSelectSerializer);

  function MultiSelectInputSerializer() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, MultiSelectInputSerializer);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = MultiSelectInputSerializer.__proto__ || Object.getPrototypeOf(MultiSelectInputSerializer)).call.apply(_ref, [this].concat(args))), _this), _this.onDidChange = function (callback) {
      return _this.observeEvent('change', (0, _utils.assertChanged)(_this, callback));
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(MultiSelectInputSerializer, [{
    key: 'value',


    /**
     * Value setter. Selects the given options.
     * Any argument value that evaluates to false will clear the selection.
     *
     * @method value
     * @param  {array|string|boolean} value
     *         New value for the related checkbox inputs. Either
     *          - an array containing values for the active options; or
     *          - string for toggling only one option.
     */

    set: function set(value) {
      var fn = void 0;
      switch (typeof value === 'undefined' ? 'undefined' : _typeof(value)) {
        case 'boolean':
          fn = function fn() {
            return value;
          };break;
        case 'string':
          fn = function fn(current) {
            return current === value;
          };break;
        default:
          fn = function fn(current) {
            return value.indexOf(current) > -1;
          };break;
      }
      this.options.forEach(function (el) {
        return el.selected = fn(el.value);
      });
    }

    /**
     * Register a change listener that is triggered whenever the value for this field changes
     * @method onDidChange
     * @param  {Function}  callback [description]
     * @return {[type]}    [description]
     */

  }]);

  return MultiSelectInputSerializer;
}(_AbstractBaseSelectSerializer2.default), _class.multiple = true, _temp2);
exports.default = MultiSelectInputSerializer;

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.serializeForm = serializeForm;

var _dom = __webpack_require__(2);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function serializeForm(form) {
  return (0, _dom.toObject)(form);
}

function accessorsFor(fieldSerializer) {
  return {
    get: function get() {
      return fieldSerializer;
    },
    set: function set(value) {
      return fieldSerializer.value = value;
    }
  };
}

function generateFieldsMap() {
  var _this = this;

  var fieldMap = (0, _dom.fields)(this.element).reduce(function (fieldMap, field) {

    var SerializerType = _this.serializer.getForField(field);
    if (!SerializerType) return fieldMap;
    var fieldSerializer = new SerializerType(field);
    return _extends(fieldMap, _defineProperty({}, field.name, accessorsFor(fieldSerializer)));
  }, {});

  Object.defineProperties(this, fieldMap);
  return Object.keys(fieldMap);
}

function setFormElementProperties() {
  var _this2 = this;

  Object.defineProperties(this.element, {
    model: { get: function get() {
        return _this2;
      } },
    fieldNames: { get: function get() {
        return _this2._fields;
      } }
  });
}

var FormModel = function () {

  /**
   * Constructs a new model for object for manipulating
   * its fields' values
   *
   * @method constructor
   * @param  {Element}   element    DOM element for the form
   * @param  {Object}    attrs.serializer An instance of SerializerRegistry class
   *                                      that will be used for resolving field
   *                                      related data.
   */

  function FormModel(el) {
    var attrs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, FormModel);

    this.element = (0, _dom.element)(el);
    this.serializer = attrs.serializer;
    this.fieldNames = generateFieldsMap.call(this);

    setFormElementProperties.call(this);
  }

  /**
   * Query for values for all fields in this form.
   *
   * @method serialize
   * @return {Object} An object with this form's fields'
   *                     names as object's keys and the fields'
   *                     respective values as object's values
   */

  _createClass(FormModel, [{
    key: 'serialize',
    value: function serialize() {
      var _this3 = this;

      return this.fieldNames.reduce(function (attr, name) {
        return _extends(attr, _defineProperty({}, name, _this3[name].value));
      }, {});
    }
  }, {
    key: 'serializedData',
    get: function get() {
      return this.serialize();
    }
  }, {
    key: 'values',
    get: function get() {
      return this.serialize();
    },
    set: function set() {
      var values = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      for (var field in values) {
        if (field in this.fieldNames) this[field] = values[field];
      }
      return this.values;
    }
  }, {
    key: 'fields',
    get: function get() {
      var _this4 = this;

      return this.fieldNames.map(function (name) {
        return _this4[name];
      });
    }
  }]);

  return FormModel;
}();

exports.default = FormModel;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FieldCollection = function FieldCollection(formElement) {
  _classCallCheck(this, FieldCollection);

  this._element = formElement;
};

exports.default = FieldCollection;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dom = __webpack_require__(2);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SerializerRegistry = function () {
  function SerializerRegistry() {
    _classCallCheck(this, SerializerRegistry);

    this.serializers = [];
  }

  _createClass(SerializerRegistry, [{
    key: 'register',
    value: function register() {
      for (var _len = arguments.length, serializers = Array(_len), _key = 0; _key < _len; _key++) {
        serializers[_key] = arguments[_key];
      }

      if (!serializers.length) throw new TypeError('SerializerRegistry.register should be called with at least one argument');
      this.serializers = this.serializers.concat(serializers);
      // serializers.forEach(s => this.serializers.set(resolveSerializerName(s), s))
    }
  }, {
    key: 'getForType',
    value: function getForType(type) {
      if (!type || typeof type !== 'string') throw new TypeError('SerializerRegistry.getForType expects a single string argument');
      return this.serializers.find(function (Serializer) {
        return Serializer.type === type;
      });
    }
  }, {
    key: 'getForField',
    value: function getForField(field) {
      field = (0, _dom.element)(field);
      if (!field) throw new TypeError('Could not parse a HTMLElement instance from the given argument in SerializerRegistry.getForField');
      return this.serializers.find(function (Serializer) {
        return Serializer.assert(field);
      });
    }
  }, {
    key: 'getAdapter',
    value: function getAdapter(field) {
      var SerializerClass = this.getForField(field);
      if (SerializerClass) return new SerializerClass(field);
      return null;
    }
  }], [{
    key: 'nameForSerializer',
    value: function nameForSerializer(SerializerClass) {
      if (SerializerClass) return resolveSerializerName(SerializerClass);
      throw new TypeError('Invalid serializer type provided for SerializerRegistry.nameForSerializer');
    }
  }]);

  return SerializerRegistry;
}();

exports.default = SerializerRegistry;


function resolveSerializerName(Serializer) {

  if (!Serializer) throw new ReferenceError('Invalid parameters while registering a serializer via SerializerRegistry.register');

  var prefix = Serializer.prefix ? Serializer.prefix.toString() : Serializer.type ? Serializer.type.toString() : Serializer.tag ? Serializer.tag.toString() : null;

  var output = function output() {
    for (var _len2 = arguments.length, parts = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      parts[_key2] = arguments[_key2];
    }

    return [prefix].concat(parts).filter(function (part) {
      return part;
    }).map(function (part) {
      return part.toLowerCase();
    }).join('-');
  };

  if (Serializer.name) return output(Serializer.name.toString());
  if (Serializer.constructor) return output(Serializer.constructor.name.toString());
  if (Serializer.id) return output(Serializer.id.toString());
  if (Serializer.key) return output(Serializer.key.toString());
  if (Serializer.name) return output(Serializer.name.toString());
  if (Serializer.title) return output(Serializer.title.toString());
  if (Serializer.identifier) return output(Serializer.identifier.toString());
  return null;
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.form = form;
exports.field = field;

var _dom = __webpack_require__(2);

var _models = __webpack_require__(4);

function form(query) {
  var elem = (0, _dom.element)(query);

  if (!elem) elem = document.body;

  if (elem.tagName !== 'FORM') elem = elem.querySelector('form');

  if (elem.model) return elem.model;

  if (!elem || !elem.querySelector) throw new Error("Element not found by query " + query);

  if (elem.tagName !== 'FORM') throw new Error("Element found by a query " + query + " is not a form");

  if (!(this && this.getAdapter)) throw new Error("Invalid serializer bound to the forms.adapter's `form` exports");

  return new _models.FormModel(elem, { serializer: this });
}

function field(query) {
  var elem = (0, _dom.element)(query);
  if (!(this && this.getAdapter)) throw new Error("Invalid serializer bound to the forms.adapter's `field` exports");
  return this.getAdapter(elem);
}

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.createSerializer = createSerializer;

var _AbstractBaseSerializer = __webpack_require__(0);

var _AbstractBaseSerializer2 = _interopRequireDefault(_AbstractBaseSerializer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var prototype = _AbstractBaseSerializer2.default.prototype;

var errorTextRegistryMissing = '\n  An instance of SerializerRegistry is required to be passed\n  as an argument to the serializer generating function';

/*
*/

/**
 * TODO: Documentation and examplery on
 *       how to use the createSerializer function
 *
 * ----------------------------------
 * Example usage with object as input
 * ----------------------------------
 * var serializer = edm.createSerializer({
 *   get: function () { return this.element.value || 'default_value' },
 *   set: function (val) { this.element.value = val },
 *   handlers: {
 *     'change': function (handler) { this.observeEvent('change', handler) },
 *     'update': function (handler) { this.observeEvent('update', handler) }
 *   }
 * })
 *
 * @method createSerializer
 *
 * @param  {SerializerRegistry}    registry     An instance of serializer registry
 * @param  {Class|Function|Object} [props=null] A serializer subclass, or property
 *                                              definitions for the subclass as an object
 *
 * @return {Serializer}                         The registered serializer
 */

function createSerializer(registry) {
  var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

  registry = props ? registry : this.register ? this : null;
  props = props || arguments[0];

  if (!registry) throw new ReferenceError(errorTextRegistryMissing);

  // Consume a class or a function
  if (props instanceof Function) return registerClass.call(registry, props);

  // Consume an object
  return constructFromObject.call(registry, props);
}

function registerClass(Serializer) {
  this.register(Serializer);
  return Serializer;
}

function constructFromObject(properties) {

  // Extract static properties
  var _properties = properties,
      tag = _properties.tag,
      type = _properties.type;

  properties = mapSerializerProps(properties);

  function Serializer() {}
  Serializer.tag = tag;
  Serializer.type = type;
  Serializer.prototype = Object.create(prototype, properties);
  Serializer.prototype.constructor = prototype.constructor;

  this.register(Serializer);
  return Serializer;
}

function mapSerializerProps(_ref) {
  var get = _ref.get,
      set = _ref.set,
      handlers = _ref.handlers;


  var handlerProps = {};
  for (var key in handlers) {
    handlerProps[key] = { value: handlers[key] };
  }
  return _extends({ value: { get: get, set: set } }, handlerProps);
}

/***/ }),
/* 27 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=edm.node.js.map
//# sourceMappingURL=edm.node.js.map