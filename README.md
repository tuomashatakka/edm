
# EDM ⏛ dom on e

Superior utilities for all things DOM.

## Getting started

### Web

To use in web environment, include the *edm* script in your html:

```<script src='dist/edm.min.js'></script>```

### Node, webpack and friends

Although using a DOM package in a **Node**-like environment makes little sense apart from when building your project for web with a bundler such as webpack, you may use *edm* in node scripts just like any other npm module by installing the *edm* from the npm:

```npm install edm --save ```,

or ```yarn add edm``` ,

or by manually including it in your project's dependencies (package.json): ```"dependencies": { ..., "edm": "^1337.0.69", ... }```

and then requiring/importing it in your scripts:

```const edm = require('edm')```,
or ```import edm from 'edm'```

### Including from a CDN

Bitch pls my project ain't not on no CDN serverss.


## Contents

The *edm* module consists of several submodules:

 - Generic DOM manipulation
 - Forms & inputs (desperately needed)
 - ...

Submodules are namespaced under the properties of the global `edm` object.

For example, to call a function under the `forms & inputs` submodule one should type

```
edm.form.[function name]()
```

where the function name is a function in the `forms & inputs` submodule.

### Submodules

#### Generic

`edm.*` & `edm()`

#### Forms & inputs

`edm.form.*`
