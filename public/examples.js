/* global edm */

var form_one = edm('#example-one')

function bindEventDispatch (eventName) {

  var container = this.element.parentElement.parentElement.querySelector('output')
  var callback = function () {

    // var args = Array.from(arguments)
    var content =
      "<h4>" + eventName + "</h4>" +
      "<strong>Value</strong> = `" +
      this.value + "`"
    // for (var arg in args) { content.push(JSON.prune(args[arg], 2, 1)) }
    // content = content.join('<br/>')
    var el = edm.dom.make(content)
    edm.dom.css(el, {
      opacity: 1,
      transition: '1s opacity',
      'white-space': 'pre-wrap'
    })

    edm.dom.insert(el, container, 0)
    setTimeout(fadeOut, 500)

    function fadeOut () {
      edm.dom.css(el, { opacity: 0 })
      setTimeout(remove, 1000)
    }

    function remove () {
      el.remove()
    }

  }

  // Listen
  this[eventName](callback)

}

bindEventDispatch.call(form_one.textinput, 'onDidChange')
bindEventDispatch.call(form_one.textinput, 'onDidStopChanging')
