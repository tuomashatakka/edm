
module.exports = function(config) {
  let webpack = require('./webpack.config')
  delete webpack.output

  config.set({
    webpack,
    singleRun: process.env.NODE_ENV === 'production',
    browsers: ['PhantomJS2'],
    files: [
      '../node_modules/babel-polyfill/dist/polyfill.js',
      { pattern: '../spec/context.js', watched: false }
    ],
    customLaunchers: {
      'PhantomJS2X': {
        base: 'PhantomJS2',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          },
        },
        flags: ['--load-images=true'],
        debug: true
      }
    },
    phantomjsLauncher: { exitOnResourceError: true },
    frameworks: ['jasmine'],
    preprocessors: { '../spec/context.js': ['webpack'] },
    webpackServer: { noInfo: true },
  })
}
