
let   conf_dev = {}
const dev      = process.env.NODE_ENV === 'development'
const npm      = process.env.NODE_ENV === 'production-node'

function sourceMap (filename) {
  return new SourceMapDevToolPlugin(
    Object.assign(SOURCE_MAP, { filename }))
}

const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const { DefinePlugin, NamedModulesPlugin, SourceMapDevToolPlugin } = webpack
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const { resolve } = require('path')

const extractPlugin = ExtractTextPlugin.extract({
  fallback: 'style-loader',
  loader:   'css-loader!less-loader',
  publicPath: '' // HACK: Required for source maps to be exported correctly
})

// const MAIN_OUTPUT_NAME  = '[name].js'
const DIST_PATH         = resolve(__dirname + '/../dist')
const ENTRY_FILE_PATH   = resolve(__dirname + '/../src/index.js')
const CSS_OUTPUT_NAME   = 'style.css'
const PUBLIC_PATH       = '/compiled'
const DEFINITIONS       = {}
const library           = 'edm'//require('../package.json').name.replace(/[^\w]+(\w)/g, (_, c) => c.toUpperCase()).toLowerCase()
const filename          = 'edm.js' // dev ? MAIN_OUTPUT_NAME : library + '.js'

const SOURCE_MAP        = {
  test:     /.*?\.jsx?/,
  filename: 'edm.js.map',
}

let plugins = [
  // new NamedModulesPlugin(),
  // new DefinePlugin(DEFINITIONS),
  new ExtractTextPlugin(CSS_OUTPUT_NAME),
]

let rules = [
  { test: /\.jsx?$/, use: 'babel-loader', exclude: /node_modules\/(?!mapbox-gl\/js)/ },
  { test: /\.less$/, use: extractPlugin }
]

let entry   = {
  [library]: ENTRY_FILE_PATH,
}

let output = {
  library,
  filename,
  path: DIST_PATH,
  libraryTarget: 'umd2'
}

if (dev) {
  output.publicPath = PUBLIC_PATH
  conf_dev = {
    devServer: {
      historyApiFallback: true,
      https: false,
      hot:   true },
    devtool: 'cheap-module-eval-source-map' }
}

else if (npm) {
  output.libraryTarget = 'commonjs2'
  output.filename = 'edm.node.js'
  plugins.push(sourceMap('[name].node.js.map'))
}

else {
  plugins.push(new UglifyJsPlugin({
    sourceMaps: true,
    minimize: true,
    // output: {
    //   comments: true,
    //   beautify: false,
    // }
  }))
  plugins.push(sourceMap('[name].js.map'))


}

let conf = {
  entry,
  output,
  plugins,
  target: "web",
  devtool: 'source-map',
  module: { rules }
}

module.exports = Object.assign({}, conf, conf_dev)
