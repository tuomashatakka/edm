
export function clear (el, type=null) {
  if (!el || !el.registeredEventHandlers)
    return el

  const remove = (kind) => {
    while (el.registeredEventHandlers[kind] && el.registeredEventHandlers[kind].length) {
        let handler = el.registeredEventHandlers[kind].pop()
        el.removeEventListener(kind, handler)
      }
    }
  if (type)
    remove(type)
  else
    Object
      .keys(el.registeredEventHandlers)
      .forEach(kind => remove(kind))
  return el
}

export function listen (el, type, handler) {
  if (!el)
    return null
  assertListenerRegistryExists(el, type)
  registerHandler(el, type, handler)
  return el
}

function registerHandler (el, type, handler) {
  el.registeredEventHandlers[type].push(handler)
  el.addEventListener(type, handler)
}

function assertListenerRegistryExists (el, type) {
  if (!el.clearListeners)
    el.clearListeners = (type=null) => clear(el, type)
  if (!el.registeredEventHandlers)
    el.registeredEventHandlers = {}
  if (!el.registeredEventHandlers[type])
    el.registeredEventHandlers[type] = []
}
