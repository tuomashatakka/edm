
const EVENT_TYPES = {
  mouse: [
    "click",
    "dblclick",
    "mousedown",
    "mouseup",
    "mouseenter",
    "mouseleave",
    "mouseover",
    "mouseout",
    "mousemove",
    "mousewheel"
  ],
  keyboard: [
    "keypress",
    "keydown",
    "keyup",
    "",
  ],
}

const EVENT_RESOLVERS = {
  keyboard: initializeKeyboardEvent,
  generic:  initializeGenericEvent,
  mouse:    initializeMouseEvent,
}

export function dispatch (el, eventName, detail={}) {
  let trigger = initializeEvent(eventName)
  return trigger(el, detail)
}

// TODO: Polyfill
// if (document.createEvent) {
//   let event = new CustomEvent(eventName, { detail })
//   return el.dispatchEvent(event)
// }
// throw new TypeError("Polyfill missing")
// else {
  // event = document.createEventObject()
  // return el.fireEvent('on' + eventName, event)
// }

function initializeEvent (type) {
  let initializer = EVENT_RESOLVERS[Object.keys(EVENT_TYPES).reduce((resolvedType, key) => {
    if (resolvedType !== null)
      return resolvedType
    if (EVENT_TYPES[key].indexOf(type) > -1)
      return key
  }, null)] || initializeGenericEvent
  return initializer(type)
}

function initializeMouseEvent (type) {
  return (el, detail) => {
    let event = document.createEvent('MouseEvent', false, false, { detail })
    event.initMouseEvent(type)
    el.dispatchEvent(event)
    return event
  }
}

function initializeKeyboardEvent (type) {
  return (el, detail) => {
    let event = document.createEvent('KeyboardEvent', false, false, { detail })
    event.initKeyboardEvent(type)
    el.dispatchEvent(event)
    return event
  }
}

function initializeGenericEvent (type) {
  return (el, detail) => {
    // let event = document.createEvent('CustomEvent')
    let event = new CustomEvent(type, { detail })

    event.initEvent(type)
    el.dispatchEvent(event)
    return event
  }
}
