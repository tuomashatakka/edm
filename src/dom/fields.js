/**
 * DOM Utility functions
 * @module dom
 */

const INPUT_TAG_TEXT      = 'input'
const INPUT_TAG_SELECT    = 'select'
const INPUT_TYPE_RADIO    = 'radio'
const INPUT_TYPE_CHECKBOX = 'checkbox'

export const isRadio      = ({ type }) => INPUT_TYPE_RADIO === type
export const isSelect     = ({ tagName }) => INPUT_TAG_SELECT === tagName.toLowerCase()
export const isCheckbox   = ({ type }) => INPUT_TYPE_CHECKBOX === type
export const isTextInput  = ({ type, tagName }) => [INPUT_TYPE_RADIO, INPUT_TYPE_CHECKBOX].indexOf(type) === -1 && INPUT_TAG_TEXT === tagName.toLowerCase()

export function value (el) {

  if (isCheckbox(el))
    return Array
      .from(el.parentElement.parentElement.querySelectorAll(`input[name="${el.name}"]`))
      .filter(el => el.checked)
      .map(el => el.value)

  if (isRadio(el)) {
    let match = Array
      .from(el.parentElement.parentElement.querySelectorAll(`input[name="${el.name}"]`))
      .find(el => el.checked)
    return match ? match.value : null
  }

  if (isSelect(el))
    return Array
      .from(el.selectedOptions)
      .map(opt => opt.value)

  return el.value
}

export function fields (el) {
  return Array.from(el.querySelectorAll('input, select, textarea'))
}

export function toObject (el) {
  return fields(el)
    .reduce((attr, field) =>
      Object.assign(attr, {[field.name]: value(field) }), {})
}
