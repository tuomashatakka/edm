
import { children, attributes } from './assignment'

/**
 * Create a new element.
 *
 * @method make
 * @param  {String} type Type of the created element; or
 *                       html contents for a new div element
 *                       in case the second argument is omitted
 * @param  {String|Object} content Either a string with html contents for the created element; or
 *                                 an object of whose properties are assigned as attributes for
 *                                 the newly created element.
 * @return {Element} The element that was created
 */

export default function make (type, attrs) {
  let html = ''

  if (typeof attrs === 'undefined') {
    attrs  = type
    type  = 'div'
  }

  if (typeof attrs === 'object') {
    html = attrs.content || attrs.html || attrs.text || attrs.children
    type = attrs.type || attrs.tag || attrs.tagName || type
    for (let key of ['content', 'children', 'html', 'text', 'type', 'tag', 'tagName']) {
      if(attrs[key])
        delete attrs[key]
    }
  }


  let el = document.createElement(type)

  if (typeof attrs === 'string')
    el.innerHTML = attrs

  else {
    if (typeof html === 'string')
      el.innerHTML = html
    else if (html instanceof HTMLCollection || html instanceof Array)
      children(el, ...html)

    if (typeof attrs === 'object')
      return attributes(el, attrs)
  }

  return el

}
