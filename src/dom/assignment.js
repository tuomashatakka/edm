import { listen } from "../events"

export function children (el, ...children) {
  for (let child of children) {
    el.appendChild(child)
  }
  return el
}

export function insert (el, p, n=null) {
  let parent = p || document.body

  if (n === null)
    return parent.appendChild(el)

  let { children } = parent
  while (n < 0)
    n += children.length
  n = n % children.length

  parent.insertBefore(el, children[n])
  return el
}

/**
 * Set a value for an attribute for the given element.
 * If value is omitted, the attribute is removed from the
 * element.
 *
 * @method attribute
 *
 * @return {Element} Target element
 */

export function attribute (el, key, val=null) {
  if (val === null)
    el.removeAttribute(key)

  else if (key.startsWith('on'))
    listen(el, key.substr(2), val)

  else
    el.setAttribute(key, val)

  return el
}

/**
 * Map attributes to an element according to given object's
 * key-value pairs.
 *
 * Keys starting with 'on' are not added as attributes but as
 * event listeners instead. E.g. `onclick` will be translated
 * to element.addEventListener('click', attr['onclick']).
 *
 * @method attributes
 *
 * @param  {Element}  el    The targeted element
 * @param  {Object}   attr  Object containing the attributes that
 *                          should be assigned to the element
 * @return {Element}        Target element
 */

export function attributes (el, attr={}) {
  Object.keys(attr).map(key => attribute(el, key, attr[key]))
  return el
}

export function css (el, style={}) {
  Object.keys(style).map(key => el.style.setProperty(key, style[key]))
  return el
}
