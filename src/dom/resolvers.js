
export default function element (query) {
  if (!query)
    return null

  let el
  let fn = [
    q => q.match(/<\w+>.*/) ? document.createElement(q.substr(q.indexOf('<'), q.indexOf(' ') - 1)) : null,
    q => q.match(/\w.*/) ? document.querySelector(`*[name='${q}']`) : null,
    q => document.querySelector(q),
    q => { // eslint-disable-line
      try { return document.createElement(q) }
      catch (e) { throw new Error("Could not parse `element(" + query + ")`") }
    }
  ]

  function test (q) {
    return fn.length ? fn.shift()(q) || test(q) : null
  }

  if (typeof query === 'function') {
    el = query()
    if (el.tagName)
      return el
  }

  if (typeof query.get === 'function')
    return query.get(0)

  if (query.tagName)
    return query

  if (typeof query === 'string')
    return test(query.trim()) || null

  else if (query.constructor.name.match(/^HTML\w+/))
    return new query()

  return null
}

export function assertElement (query) {
  return element(query) ? true : false
}
