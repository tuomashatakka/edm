import { element } from '../../dom'

export default class SerializerRegistry {

  constructor () {
    this.serializers = []
  }

  register (...serializers) {
    if (!serializers.length)
      throw new TypeError(`SerializerRegistry.register should be called with at least one argument`)
    this.serializers = this.serializers.concat(serializers)
    // serializers.forEach(s => this.serializers.set(resolveSerializerName(s), s))
  }

  getForType (type) {
    if (!type || typeof type !== 'string')
      throw new TypeError(`SerializerRegistry.getForType expects a single string argument`)
    return this.serializers.find(Serializer => Serializer.type === type)
  }

  getForField (field) {
    field = element(field)
    if (!field)
      throw new TypeError(`Could not parse a HTMLElement instance from the given argument in SerializerRegistry.getForField`)
    return this.serializers.find(Serializer => Serializer.assert(field))
  }

  getAdapter (field) {
    let SerializerClass = this.getForField(field)
    if (SerializerClass)
      return new SerializerClass(field)
    return null
  }

  static nameForSerializer (SerializerClass) {
    if (SerializerClass)
      return resolveSerializerName(SerializerClass)
    throw new TypeError(`Invalid serializer type provided for SerializerRegistry.nameForSerializer`)
  }
}


function resolveSerializerName (Serializer) {

  if (!Serializer)
    throw new ReferenceError(`Invalid parameters while registering a serializer via SerializerRegistry.register`)

  const prefix =
      Serializer.prefix
    ? Serializer.prefix.toString()
    : Serializer.type
    ? Serializer.type.toString()
    : Serializer.tag
    ? Serializer.tag.toString()
    : null

  const output = (...parts) => [ prefix ]
    .concat(parts)
    .filter(part => part)
    .map(part => part.toLowerCase())
    .join('-')

  if (Serializer.name)
    return output(Serializer.name.toString())
  if (Serializer.constructor)
    return output(Serializer.constructor.name.toString())
  if (Serializer.id)
    return output(Serializer.id.toString())
  if (Serializer.key)
    return output(Serializer.key.toString())
  if (Serializer.name)
    return output(Serializer.name.toString())
  if (Serializer.title)
    return output(Serializer.title.toString())
  if (Serializer.identifier)
    return output(Serializer.identifier.toString())
  return null
}
