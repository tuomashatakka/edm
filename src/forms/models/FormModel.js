import { toObject, fields, element } from '../../dom'

export function serializeForm (form) {
  return toObject(form)
}

function accessorsFor (fieldSerializer) {
  return {
    get: ()      => fieldSerializer,
    set: (value) => fieldSerializer.value = value
  }
}

function generateFieldsMap () {

  let fieldMap = fields(this.element)
    .reduce((fieldMap, field) => {

      let SerializerType = this.serializer.getForField(field)
      if (!SerializerType)
        return fieldMap
      let fieldSerializer = new SerializerType(field)
      return Object.assign(fieldMap, { [field.name]: accessorsFor(fieldSerializer) })
    }, {})

  Object.defineProperties(this, fieldMap)
  return Object.keys(fieldMap)
}

function setFormElementProperties () {
  Object.defineProperties(this.element, {
    model:  { get: () => this },
    fieldNames: { get: () => this._fields },
  })
}


export default class FormModel {

  /**
   * Constructs a new model for object for manipulating
   * its fields' values
   *
   * @method constructor
   * @param  {Element}   element    DOM element for the form
   * @param  {Object}    attrs.serializer An instance of SerializerRegistry class
   *                                      that will be used for resolving field
   *                                      related data.
   */

  constructor (el, attrs={}) {
    this.element    = element(el)
    this.serializer = attrs.serializer
    this.fieldNames = generateFieldsMap.call(this)

    setFormElementProperties.call(this)
  }

  /**
   * Query for values for all fields in this form.
   *
   * @method serialize
   * @return {Object} An object with this form's fields'
   *                     names as object's keys and the fields'
   *                     respective values as object's values
   */

  serialize () {
    return this
      .fieldNames
      .reduce((attr, name) => Object.assign(attr, {[name]: this[name].value }), {})
  }

  get serializedData () {
    return this.serialize()
  }

  get values () {
    return this.serialize()
  }

  get fields () {
    return this
      .fieldNames
      .map(name => this[name])
  }

  set values (values={}) {
    for (let field in values) {
      if (field in this.fieldNames)
        this[field] = values[field]
    }
    return this.values
  }

}
