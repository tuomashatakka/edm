
import BaseSerializer from './serializers/AbstractBaseSerializer'

const { prototype } = BaseSerializer
const errorTextRegistryMissing = `
  An instance of SerializerRegistry is required to be passed
  as an argument to the serializer generating function`

/*
*/

/**
 * TODO: Documentation and examplery on
 *       how to use the createSerializer function
 *
 * ----------------------------------
 * Example usage with object as input
 * ----------------------------------
 * var serializer = edm.createSerializer({
 *   get: function () { return this.element.value || 'default_value' },
 *   set: function (val) { this.element.value = val },
 *   handlers: {
 *     'change': function (handler) { this.observeEvent('change', handler) },
 *     'update': function (handler) { this.observeEvent('update', handler) }
 *   }
 * })
 *
 * @method createSerializer
 *
 * @param  {SerializerRegistry}    registry     An instance of serializer registry
 * @param  {Class|Function|Object} [props=null] A serializer subclass, or property
 *                                              definitions for the subclass as an object
 *
 * @return {Serializer}                         The registered serializer
 */

export function createSerializer (registry, props=null) {
  registry = props ? registry : this.register ? this : null
  props    = props || arguments[0]

  if (!registry)
    throw new ReferenceError(errorTextRegistryMissing)

  // Consume a class or a function
  if (props instanceof Function)
    return registerClass.call(registry, props)

  // Consume an object
  return constructFromObject.call(registry, props)
}


function registerClass (Serializer) {
  this.register(Serializer)
  return Serializer
}


function constructFromObject (properties) {

  // Extract static properties
  let { tag, type } = properties
  properties = mapSerializerProps(properties)

  function Serializer () { }
  Serializer.tag  = tag
  Serializer.type = type
  Serializer.prototype = Object.create(prototype, properties)
  Serializer.prototype.constructor = prototype.constructor

  this.register(Serializer)
  return Serializer
}


function mapSerializerProps ({ get, set, handlers }) {

  let handlerProps = {}
  for (let key in handlers) {
    handlerProps[key] = { value: handlers[key] }
  }
  return { value: { get, set }, ...handlerProps }
}
