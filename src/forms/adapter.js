
import { element } from '../dom'
import { FormModel } from './models'

export function form (query) {
  let elem = element(query)

  if (!elem)
    elem = document.body

  if (elem.tagName !== 'FORM')
    elem = elem.querySelector('form')

  if (elem.model)
    return elem.model

  if (!elem || !elem.querySelector)
    throw new Error("Element not found by query " + query)

  if (elem.tagName !== 'FORM')
    throw new Error("Element found by a query " + query + " is not a form")

  if (!(this && this.getAdapter))
    throw new Error("Invalid serializer bound to the forms.adapter's `form` exports")

  return new FormModel(elem, { serializer: this })
}

export function field (query) {
  let elem = element(query)
  if (!(this && this.getAdapter))
    throw new Error("Invalid serializer bound to the forms.adapter's `field` exports")
  return this.getAdapter(elem)
}
