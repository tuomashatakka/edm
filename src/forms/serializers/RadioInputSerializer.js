
import Serializer from './AbstractBaseSerializer'
import { assertChanged } from '../../utils'


export default class RadioInputSerializer extends Serializer {

  static tag  = 'input'
  static type = 'radio'

  /**
   * Returns all radio inputs within the given field's form. If the input has
   * no form, whole document is used to search for the inputs.
   * @method element
   * @return {Array} List of all related checkbox input elements
   */

  get element () {
    let { type } = this.constructor
    let { name, form } = this._element

    return (form || document.body)
      .querySelectorAll(`input[type="${type}"][name="${name}"]`)
  }

  /**
   * Returns a list of values within related input elements
   * @method options
   * @return {[type]} [description]
   */

  get options () {
    return Array
      .from(this.element)
      .map(el => el.value)
  }

  /**
   * Value getter. Returns an array containing values for each active checkbox within related inputs
   * @method value
   * @return {Array} List of active checkboxes' values
   */

  get value () {
    let active = Array
      .from(this.element)
      .filter(el => el.checked)
    if (active.length)
      return active[0].value
    return null
  }

  /**
   * Value setter. Changes the related checkboxes' states according to provided value
   *
   * @method value
   * @param  {string|number} value New value for the field
   *                               Either the value for the input that should be marked active
   *                               or a number describing the zero-based index for the input to be marked active
   *                               On any other case all inputs are set unselected
   */

  set value (value) {
    let fn
    switch (typeof value) {
      case 'number':  fn = (el, n) => n === value; break
      case 'string':  fn = current => current === value; break
      default:        fn = () => false; break
    }
    this.element.forEach((el, n) => el.checked = fn(el.value, n))
  }

  /**
   * Register a change listener that is triggered whenever the value for this field changes
   * @method onDidChange
   * @param  {Function}  callback [description]
   * @return {[type]}    [description]
   */

  onDidChange = (callback) => this.observeEvent('click', assertChanged(this, callback))

}
