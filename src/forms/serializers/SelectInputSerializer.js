
import BaseSelectSerializer from './AbstractBaseSelectSerializer'
import { assertChanged } from '../../utils'


export default class SelectInputSerializer extends BaseSelectSerializer {

  /**
   * Returns a list of values within related input elements
   * @method options
   * @return {[type]} [description]
   */

  get options () {
    return Array
      .from(this.element.options)
      .map(option => option.value)
  }

  /**
   * Value getter. Returns an array containing all selected options
   * @method value
   * @return {Array} List of active checkboxes' values
   */

  get value () {
    return Array
      .from(this.element.selectedOptions)
      .map(opt => opt.value)
  }

  /**
   * Value setter. Selects the given options.
   * Any argument value that evaluates to false will clear the selection.
   *
   * @method value
   * @param  {array|string|boolean} value
   *         New value for the related checkbox inputs. Either
   *          - an array containing values for the active options; or
   *          - string for toggling only one option.
   */

  set value (value) {
    let optEls = [...this.element.options]
    let fn

    if (!value)
      fn = fn = () => value

    else
      switch (typeof value) {
        case 'boolean': fn = () => value; break
        case 'number':  fn = (cr, n) => value === n; break
        case 'string':  fn = current => current === value; break
        case 'object':  fn = current => resolveObject(value, current); break
        default:        fn = () => false
      }

    optEls.forEach(el => el.selected = fn(el.value))
  }

  /**
   * Register a change listener that is triggered whenever the value for this field changes
   * @method onDidChange
   * @param  {Function}  callback [description]
   * @return {[type]}    [description]
   */

  onDidChange = (callback) => this.observeEvent('change', assertChanged(this, callback))

}


function resolveObject (value, current) {
  if (value.constructor.name === 'Array')
    return value.indexOf(current) > -1
  return value[current]
}
