
import { Collection, Event } from 'disposable-events'
import CustomEvent from 'custom-event'

export default class BaseFieldSerializer {

  static tag  = null
  static type = null

  static assert (field) {
    return (
      field.tagName.toLowerCase() === this.tag &&
      field.getAttribute('type') === this.type
    )
  }

  constructor (field) {
    this._element = field
    this.subscriptions = new Collection()
  }

  get tagName () {
    return this.constructor.tag
  }

  get inputType () {
    return this.constructor.type
  }

  get element () {
    return this._element
  }

  get value () {
    throw new Error(`Subclasses of ${this.constructor.name} must define a getter method for the \`value\` property`)
  }

  set value (value) {
    throw new Error(`Subclasses of ${this.constructor.name} must define a setter method for the \`value\` property`)
  }

  destroy () {
    this.subscriptions.dispose()
    this.element.remove()
  }

  onChange (handler) {
    return event => {
      if (handler(event, this) !== false) {
        let data = {
          event,
          serializer: this
        }
        let callbackEvent = new CustomEvent('update', data)
        this.element.dispatchEvent(callbackEvent)
      }
    }
  }

  observeEvent () {
    return observeEvent.apply(this, arguments)
  }

}

function observeEvent (type, callback) {
  let disposables
  let target  = this.element
  let handler = this.onChange(callback)
  if (target.length)
    target = [ ...target ]
  if (target.map)
    disposables = target.map(el => new Event({ type, handler, target: el }))
  else
    disposables = [new Event({ type, target, handler })]
  this.subscriptions.add(...disposables)
}
