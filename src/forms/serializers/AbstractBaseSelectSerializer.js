
import Serializer from './AbstractBaseSerializer'

export default class BaseSelectSerializer extends Serializer {

  static tag  = 'select'
  static multiple = false

  static assert (field) {
    return (
      this.tag === field.tagName.toLowerCase()
      // &&
      // this.multiple === !!field.getAttribute('multiple')
    )
  }

}
