
import TextInputSerializer from './TextInputSerializer'


export default class TextareaSerializer extends TextInputSerializer {

  static tag  = 'textarea'

  static assert (field) {
    let type = field.getAttribute('type')
    return (
      field.tagName.toLowerCase() === this.tag &&
      type === null
    )
  }

}
