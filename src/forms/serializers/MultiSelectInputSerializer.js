
import BaseSelectSerializer from './AbstractBaseSelectSerializer'
import { assertChanged } from '../../utils'


export default class MultiSelectInputSerializer extends BaseSelectSerializer {

  static multiple = true

  /**
   * Value setter. Selects the given options.
   * Any argument value that evaluates to false will clear the selection.
   *
   * @method value
   * @param  {array|string|boolean} value
   *         New value for the related checkbox inputs. Either
   *          - an array containing values for the active options; or
   *          - string for toggling only one option.
   */

  set value (value) {
    let fn
    switch (typeof value) {
      case 'boolean': fn = () => value; break
      case 'string':  fn = current => current === value; break
      default:        fn = current => value.indexOf(current) > -1; break
    }
    this.options.forEach(el => el.selected = fn(el.value))
  }

  /**
   * Register a change listener that is triggered whenever the value for this field changes
   * @method onDidChange
   * @param  {Function}  callback [description]
   * @return {[type]}    [description]
   */

  onDidChange = (callback) => this.observeEvent('change', assertChanged(this, callback))

}
