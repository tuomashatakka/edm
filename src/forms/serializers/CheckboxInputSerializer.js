
import Serializer from './AbstractBaseSerializer'
import { assertChanged } from '../../utils'


export default class CheckboxInputSerializer extends Serializer {

  static tag  = 'input'
  static type = 'checkbox'

  /**
   * Returns all checkbox inputs within the given field's form. If the input has
   * no form, whole document is used to search for the inputs.
   * @method element
   * @return {Array} List of all related checkbox input elements
   */

  get element () {
    let { name, form } = this._element
    let { type } = this.constructor
    return form.querySelectorAll(`input[type="${type}"][name="${name}"]`)
  }

  /**
   * Returns a list of values within related input elements
   * @method options
   * @return {[type]} [description]
   */

  get options () {
    return Array
      .from(this.element)
      .map(el => el.value)
  }

  /**
   * Value getter. Returns an array containing values for each active checkbox within related inputs
   * @method value
   * @return {Array} List of active checkboxes' values
   */

  get value () {
    return Array
      .from(this.element)
      .filter(el => el.checked)
      .map(el => el.value)
  }

  /**
   * Value setter. Changes the related checkboxes' states according to provided value
   *
   * @method value
   * @param  {array|string|boolean} value New value for the related checkbox inputs
   *                                      Either an array containing values for the active checkboxes
   *                                      or string for toggling only one checkbox
   *                                      or boolean to toggle uniform checked state for each checkbox
   */

  set value (value) {
    let fn
    switch (typeof value) {
      case 'boolean': fn = () => value; break
      case 'string':  fn = current => current === value; break
      default:        fn = current => value.indexOf(current) > -1; break
    }
    this.element.forEach(el => el.checked = fn(el.value))
  }

  /**
   * Register a change listener that is triggered whenever the value for this field changes
   * @method onDidChange
   * @param  {Function}  callback [description]
   * @return {[type]}    [description]
   */

  onDidChange = (callback) => this.observeEvent('click', assertChanged(this, callback))

}
