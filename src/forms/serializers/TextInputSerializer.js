
import Serializer from './AbstractBaseSerializer'
import { throttle, assertChanged } from '../../utils'

export default class TextInputSerializer extends Serializer {
  static tag  = 'input'
  static type = 'text'
  static assert (field) {
    let type = field.getAttribute('type')
    return (
      field.tagName.toLowerCase() === this.tag &&
      ['checkbox', 'radio'].indexOf(type) === -1
    )
  }

  get value () {
    return this.element.value
  }

  set value (value) {
    this.element.value = value
    this.element.dispatchEvent(new Event('keyup'))
  }

  onDidChange = (callback) =>
    this.observeEvent('keyup', assertChanged(this, callback))

  onDidStopChanging = (callback) =>
    this.observeEvent('keyup', throttle(assertChanged(this, callback)))

}
