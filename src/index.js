
import * as dom from './dom'
import * as forms from './forms'
import * as model from './forms/models'
import * as event from './events'
import { serializers } from './forms'
import * as connect from './forms/adapter'
import { createSerializer } from './forms/extend'

import './style.less'

const registry = new forms.SerializerRegistry()

const field    = connect.field.bind(registry)
const form     = connect.form.bind(registry)

const defaultSerializers = [
  serializers.TextInput,
  serializers.Textarea,
  serializers.Checkbox,
  serializers.Radio,
  serializers.Select
]

const properties = {
  dom:    _prop(dom),
  form:   _prop(form),
  field:  _prop(field),
  model:  _prop(model),
  event:  _prop(event),

  serializerTypes:    _prop(serializers),
  createSerializer:   _prop(createSerializer.bind(registry)),
  registerSerializer: _prop(registry.register.bind(registry)),
}

function edm () {
  return form.apply(this, arguments)
}

function _prop (val) {
  return { value: val }
}

// Exports
registry.register(...defaultSerializers)
Object.defineProperties(edm, properties)
module.exports = edm
