
const DEFAULT_THROTTLE_INTERVAL = 550


export function throttle (callback, interval) {

  let timeout = null
  interval = isNaN(parseInt(interval)) ? DEFAULT_THROTTLE_INTERVAL : interval

  return function () {
    let handler = () => callback.apply(this, arguments)
    clearTimeout(timeout)
    timeout = setTimeout(handler, interval)
  }
}

export function assertChanged (self, callback) {

  let _previousValue = null

  return function () {

    let val = self.value
    let doDispatch = true

    if (typeof val === 'string')
      doDispatch = val !== _previousValue
    else if (typeof val === 'boolean')
      doDispatch = val !== _previousValue
    else
      doDispatch = !_previousValue || val && (val.length != _previousValue.length)

    if (doDispatch) {
      _previousValue = val
      return callback.apply(self, arguments)
    }
  }
}
